echo START

echo ROLLBACK TO DATE 'yyyy-MM-dd'
call gradlew task %1 rollbackToDate %2
set logfile = %1
echo type "log\%logfile%.log"
echo ROLLBACK FINISHED

echo FINISH
pause