echo START

echo ROLLBACK 1 CHANGESET
call gradlew task %1 rollbackCount 1
set logfile = %1
echo type "log\%logfile%.log"
echo ROLLBACK FINISHED

echo FINISH
pause