/*
 *  Copyright (C) 2010 {Apertum}Projects. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.apertum.qsky2.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.HashSet;
import java.util.Set;

/**
 * @author egorov
 */
@Entity
@Table(name = "branch")
@Getter
@Setter
@NoArgsConstructor
public class Branch extends Element {

    @Column(name = "branch_id", nullable = false, unique = true)
    private Long branchId;
    @Column(name = "name", length = 254, nullable = false, columnDefinition = "")
    private String name;
    @Column(name = "active", columnDefinition = "false", nullable = false)
    private Boolean active = true;
    @Column(name = "time_zone")
    private Integer timeZone = 0;
    /**
     * The parent domain, can be null if this is the root domain.
     */
    @ManyToOne(cascade = {CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "parent_id")
    private Branch parent;
    /**
     * The children domain of this domain.
     * <p>
     * This is the inverse side of the parent relation.
     *
     * <strong>It is the children responsibility to manage there parents children set!</strong>
     */
    //@OneToMany(fetch = FetchType.LAZY)
    //@JoinColumn(name = "parent_id")
    @Transient
    private Set<Branch> children = new HashSet<>();

    public Branch(String name) {
        this.name = name;
    }

    /**
     * Instantiates a new domain. The domain will be of the same state like the parent domain.
     *
     * @param parent the parent domain
     */
    public Branch(final Branch parent) {
        if (parent == null) {
            throw new IllegalArgumentException("parent required");
        }

        this.parent = parent;
        registerInParentsChilds();
    }

    /**
     * Register this domain in the child list of its parent.
     */
    private void registerInParentsChilds() {
        this.parent.children.add(this);
    }

    /**
     * Move this domain to an new parent domain.
     *
     * @param newParent the new parent
     */
    public void move(final Branch newParent) {
        this.parent.children.remove(this);
        this.parent = newParent;
        registerInParentsChilds();
    }

    public boolean isParentOf(Branch br) {
        while (br.getParent() != null && !br.getParent().getId().equals(getId())) {
            br = br.getParent();
        }
        return br.getParent() != null;
    }

    @Override
    public String toString() {
        return name + "(" + branchId.toString() + ")";
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Branch && getBranchId().equals(((Branch) o).getBranchId());
    }

    @Override
    public int hashCode() {
        return getBranchId().hashCode();
    }
}
