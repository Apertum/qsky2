/*
 *  Copyright (C) 2010 {Apertum}Projects. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.apertum.qsky2.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * @author egorov
 */
@Entity
@Table(name = "customer")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Customer extends Element {

    @Column(name = "number")
    private Integer number;
    @Column(name = "service_prefix")
    private String prefix;
    @Column(name = "visit_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date visitTime;
    @Column(name = "waiting", nullable = false)
    private Long waiting = 0L;
    @Column(name = "working", nullable = false)
    private Long working = 0L;
    //***************************************************************************************************************
    @Column(name = "branch_id")
    private Long branchId;
    @Column(name = "customer_id")
    private Long customerId;
    @Column(name = "service_id")
    private Long serviceId;
    @Column(name = "employee_id")
    private Long employeeId;
    //***************************************************************************************************************
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "before_customer_id", referencedColumnName = "id")
    private Customer before;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "after_customer_id", referencedColumnName = "id")
    private Customer after;
    //***************************************************************************************************************
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "first_step_id", referencedColumnName = "id")
    private Step firstStep;
    @Column(name = "present_state")
    private Integer state;

    public Customer(Long branchId, Long customerId) {
        this.branchId = branchId;
        this.customerId = customerId;
        this.visitTime = new Date();
    }
}
