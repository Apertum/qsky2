/*
 *  Copyright (C) 2010 {Apertum}Projects. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.apertum.qsky2.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author egorov
 */
@Entity
@Table(name = "service")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class QSkySrv extends Element {

    @Column(name = "name", length = 255, nullable = false, columnDefinition = "")
    private String name;
    @Column(name = "branch_id")
    private Long branchId;
    @Column(name = "service_id")
    private Long serviceId;

    public QSkySrv(Long branchId, Long serviceId, String name) {
        this.name = name;
        this.branchId = branchId;
        this.serviceId = serviceId;
    }
}
