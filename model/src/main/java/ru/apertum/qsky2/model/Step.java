/*
 *  Copyright (C) 2010 {Apertum}Projects. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.apertum.qsky2.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * @author egorov
 */
@Entity
@Table(name = "step")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Step extends Element {

    @Column(name = "branch_id", nullable = false)
    private Long branchId;
    @Column(name = "customer_id", nullable = false)
    private Long customerId;
    @Column(name = "service_id")
    private Long serviceId;
    @Column(name = "employee_id")
    private Long employeeId;
    //***************************************************************************************************************
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumns({
            @JoinColumn(name = "before_step_id", referencedColumnName = "id")
    })
    private Step before;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumns({
            @JoinColumn(name = "after_step_id", referencedColumnName = "id")
    })
    private Step after;
    //***************************************************************************************************************
    @Column(name = "stand_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date standTime;
    @Column(name = "start_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;
    @Column(name = "finish_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finishTime;
    @Column(name = "waiting", nullable = false)
    private Long waiting = 0L;
    @Column(name = "working", nullable = false)
    private Long working = 0L;
    @Column(name = "start_state")
    private Integer startState;
    @Column(name = "finish_state")
    private Integer finishState;

    public Step(Long branchId, Long customerId) {
        this.branchId = branchId;
        this.customerId = customerId;
    }

    public Step getLastStep() {
        Step last = this;
        while (last.getAfter() != null) {
            last = last.getAfter();
        }
        return last;
    }

    public int getStepsCount() {
        int res = 1;
        Step last = this;
        while (last.getAfter() != null) {
            last = last.getAfter();
            res++;
        }
        return res;
    }

    @Override
    public String toString() {
        return branchId + "/" + serviceId + "/" + employeeId;
    }
}
