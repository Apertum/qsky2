package ru.apertum.qsky2.auth;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Optional;

@RequiredArgsConstructor
@RestController
@Log4j2
@RequestMapping("/qsky2seq")
public class AuthController {

    private final AuthService service;

    @GetMapping(value = "auth", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<AuthResponse>> auth(@RequestHeader(name = "qsky2-auth-token") String token, @RequestHeader(name = "qsky2-plugin-version") String version) {
        final Mono<AuthResponse> objectMono = Mono.fromCallable(() -> service.identify(version, token));
        return objectMono.map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.of(Optional.of(new AuthResponse(404, null, null))))
                .doOnError(throwable -> log.error("Unable to identify by token={}; ver={}", token, version, throwable))
                .onErrorReturn(ResponseEntity.of(Optional.of(new AuthResponse(500, null, null))));
    }

    @GetMapping(value = "verify")
    public ResponseEntity verify(@RequestHeader(name = "qsky2-token") String token) {
        return new ResponseEntity(service.verify(token) ? HttpStatus.ACCEPTED : HttpStatus.NOT_ACCEPTABLE);
    }

    @GetMapping(value = "allrefresh")
    public void refresh(@RequestParam(name = "all-refresh-auth") String str) {
        service.refreshAuth();
    }

    @GetMapping(value = "ping")
    public String ping() {
        return "pong";
    }

    @GetMapping(value = "state")
    public String state() {
        return "<html><head><meta charset=\"utf-8\"/><title>Auth state</title></head><body><h1>Auth state</h1><span>Could be ok.</span></body>";
    }
}
