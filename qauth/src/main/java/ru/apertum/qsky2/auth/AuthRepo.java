package ru.apertum.qsky2.auth;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
@RequiredArgsConstructor
@Log4j2
public class AuthRepo {

    private final JdbcTemplate jdbc;

    public AuthData getAuthData(String token) {
        try {
            return jdbc.queryForObject("SELECT endpoint_api, expire, start FROM auth where token_auth=?", new Object[]{token},
                    (rs, rowNum) -> new AuthData(rs.getString("endpoint_api"), rs.getDate("start"), rs.getDate("expire")));
        } catch (IncorrectResultSizeDataAccessException ex) {
            return null;
        } catch (Exception ex) {
            log.catching(ex);
            return null;
        }
    }

    @Data
    public static class AuthData {
        private final String endpoint;
        private final Date start;
        private final Date expire;

        public boolean isActive() {
            if (start == null && expire == null) {
                return true;
            } else {
                final Date now = new Date();
                if (start == null) {
                    return now.compareTo(expire) <= 0;
                }
                if (expire == null) {
                    return start.compareTo(now) <= 0;
                }
                return start.compareTo(now) * now.compareTo(expire) >= 0;
            }
        }
    }
}
