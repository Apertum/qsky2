package ru.apertum.qsky2.auth;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class AuthResponse {
    private final int status;
    private final String token;
    private final String endpoint;

    public AuthResponse(int status) {
        this.status = status;
        token = null;
        endpoint = null;
    }
}
