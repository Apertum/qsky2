
package ru.apertum.qsky2.auth;


import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.io.CharStreams;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
@Log4j2
public class AuthService {

    private final AuthRepo repo;

    private LoadingCache<String, Boolean> tokens;
    private LoadingCache<AuthReq, AuthResponse> permissions;

    public AuthResponse identify(String version, String token) {
        try {
            return permissions.get(new AuthReq(version, token));
        } catch (ExecutionException e) {
            log.catching(e);
            return new AuthResponse(500, null, null);
        }
    }

    public boolean verify(String token) {
        try {
            return tokens.get(token);
        } catch (ExecutionException e) {
            log.catching(e);
            return false;
        }
    }

    @PostConstruct
    private void init() {
        tokens = CacheBuilder.newBuilder().maximumSize(1_000).expireAfterAccess(30, TimeUnit.MINUTES).build(new CacheLoader<String, Boolean>() {
            @Override
            public Boolean load(String token) {
                log.debug("Token '{}' rejected", token);
                return false; // елси пришлось генерировать из-за отсутсвия, то явно что не положили при идентификации.
            }
        });

        permissions = CacheBuilder.newBuilder().maximumSize(1_000)
                .expireAfterWrite(1, TimeUnit.HOURS).expireAfterAccess(30, TimeUnit.MINUTES)
                .build(new CacheLoader<AuthReq, AuthResponse>() {
                    @Override
                    public AuthResponse load(AuthReq authReq) {
                        final AuthResponse auth = getAuthResponseFromDb(authReq.getToken());
                        if (auth.getStatus() == HttpStatus.OK.value()) {
                            tokens.put(auth.getToken(), true); // Сохраним как правильный, но его еще надо проверить на совместимость версий.
                            if (isVersionSupported(auth.getEndpoint(), authReq.getVersion())) {
                                log.info("Auth was success for {} -> {}", authReq, auth);
                                return auth;
                            } else {
                                log.debug("Auth was failed as UPGRADE_REQUIRED for {} -> {}", authReq, auth);
                                return new AuthResponse(HttpStatus.UPGRADE_REQUIRED.value());
                            }
                        } else {
                            log.debug("Auth was failed with status {} for {} -> {}", auth.getStatus(), authReq, auth);
                            return auth;
                        }
                    }
                });
    }

    private boolean isVersionSupported(String endpoint, String version) {
        final HttpResponse response;
        try {
            response = Request.Get(endpoint + "/ping/?version=" + version).execute().returnResponse();
        } catch (Exception e) {
            log.error(e);
            return false;
        }
        if (response.getStatusLine().getStatusCode() == HttpStatus.OK.value()) {
            try {
                final String result = CharStreams.toString(new InputStreamReader(response.getEntity().getContent(), StandardCharsets.UTF_8));
                return "1".equals(result);
            } catch (IOException e) {
                log.catching(e);
                return false;
            }
        } else {
            log.warn("Version {} is not verified on {}. Code response = {}", version, endpoint, response.getStatusLine().getStatusCode());
            return false;
        }
    }

    private AuthResponse getAuthResponseFromDb(String token) {
        final AuthRepo.AuthData data = repo.getAuthData(token);
        if (data != null && data.isActive()) {
            return new AuthResponse(HttpStatus.OK.value(), generateToken(token), data.getEndpoint());
        } else {
            return new AuthResponse(HttpStatus.FORBIDDEN.value());
        }
    }

    private String generateToken(String token) {
        return token;
    }

    public void refreshAuth() {
        permissions.invalidateAll();
    }


    @Data
    public static class AuthReq {
        private final String version;
        private final String token;
    }
}
