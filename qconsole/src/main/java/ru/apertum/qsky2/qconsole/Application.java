package ru.apertum.qsky2.qconsole;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackages = {"ru.apertum.qsky2"})
@Log4j2
public class Application implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("QConsole was started\nGO! GO! GO!\nGO! GO! GO!\nGO! GO! GO!\nGO! GO! GO!");
    }
}
