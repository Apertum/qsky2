package ru.apertum.qsky2.qconsole.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;
import ru.apertum.qsky2.model.Branch;
import ru.apertum.qsky2.qconsole.controller.branch_tree.AdvancedTreeModel;
import ru.apertum.qsky2.qconsole.controller.branch_tree.BranchTreeNode;
import ru.apertum.qsky2.qconsole.core.ConsoleRepo;

import java.util.ArrayList;
import java.util.List;

@Service("branch")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@RequiredArgsConstructor
public class BranchService {

    private final ConsoleRepo repo;

    @Bean("tree")
    @Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public AdvancedTreeModel getAdvancedTreeModel(@Autowired BranchTreeNode root) {
        return new AdvancedTreeModel(root);
    }

    @Bean("root")
    @Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public BranchTreeNode getRoot() {
        final List<Branch> list = repo.getBranches();
        final List<BranchTreeNode> roots = new ArrayList<>();
        list.forEach(branch -> {
            if (branch.getParent() == null) {
                BranchTreeNode treeNode = new BranchTreeNode(branch, getChildren(list, branch));
                roots.add(treeNode);
            }
        });
        return new BranchTreeNode(null, roots.toArray(new BranchTreeNode[0]));
    }

    private BranchTreeNode[] getChildren(final List<Branch> list, final Branch parent) {
        final List<BranchTreeNode> children = new ArrayList<>();
        list.forEach(candidat -> {
            if (parent.equals(candidat.getParent())) {
                parent.getChildren().add(candidat);
                BranchTreeNode treeNode = new BranchTreeNode(candidat, getChildren(list, candidat));
                children.add(treeNode);
            }
        });
        return children.toArray(new BranchTreeNode[0]);
    }
}
