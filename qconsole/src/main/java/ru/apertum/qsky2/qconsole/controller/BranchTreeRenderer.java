package ru.apertum.qsky2.qconsole.controller;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.zkoss.zk.ui.event.DropEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Image;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.TreeitemRenderer;
import org.zkoss.zul.Treerow;
import org.zkoss.zul.Window;
import ru.apertum.qsky2.model.Branch;
import ru.apertum.qsky2.qconsole.controller.branch_tree.AdvancedTreeModel;
import ru.apertum.qsky2.qconsole.controller.branch_tree.BranchTreeNode;
import ru.apertum.qsky2.qconsole.core.ConsoleRepo;
import ru.apertum.qsky2.qconsole.core.User;

import javax.sql.rowset.serial.SerialException;

@Component("renderer")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@RequiredArgsConstructor
public class BranchTreeRenderer implements TreeitemRenderer<BranchTreeNode> {

    private final User user;

    private final AdvancedTreeModel treeModel;

    private final ConsoleRepo repo;

    @Getter
    @Setter
    private Window branchPropsDialog;

    @Override
    public void render(final Treeitem treeItem, BranchTreeNode treeNode, int index) {
        final Branch branch = treeNode.getData();
        final Treerow dataRow = new Treerow();
        dataRow.setParent(treeItem);
        treeItem.setValue(treeNode);
        treeItem.setOpen(treeNode.isOpen());

        final Hlayout hl = new Hlayout();
        hl.appendChild(new Image("/resources/img/label32.png"));
        hl.appendChild(new Label(branch.getName()));
        hl.setSclass("h-inline-block");
        final Treecell treeCell = new Treecell();
        treeCell.appendChild(hl);
        dataRow.setDraggable("true");
        dataRow.appendChild(treeCell);
        dataRow.appendChild(new Treecell("" + branch.getBranchId() + " " + (branch.getActive() ? "Active" : "Disable")));
        dataRow.addEventListener(Events.ON_DOUBLE_CLICK, (Event event) -> {
            final BranchTreeNode clickedNodeValue = ((Treeitem) event.getTarget().getParent()).getValue();
            final Branch br = clickedNodeValue.getData();
            final Window w = branchPropsDialog;
            w.setTitle(br.toString());
            ((Textbox) w.getFellow("nameBranch")).setText(br.getName());
            ((Intbox) w.getFellow("idBranch")).setValue(br.getBranchId().intValue());
            ((Intbox) w.getFellow("timeZone")).setValue(br.getTimeZone());
            w.setVisible(true);
            w.setPosition("center");
            w.doModal();
        });

        // Both category row and contact row can be item dropped
        dataRow.setDroppable("true");
        dataRow.addEventListener(Events.ON_DROP, (Event event) -> {
            // The dragged target is a TreeRow belongs to an
            // Treechildren of TreeItem.
            final Treeitem draggedItem = (Treeitem) ((DropEvent) event).getDragged().getParent();
            final BranchTreeNode draggedValue = draggedItem.getValue();

            // разрешение переносить корневые ветки
            if (draggedValue.getData().getParent() == null && !user.getBranches().isEmpty()) {
                return;
            }
            if (!draggedValue.isParentOf(treeItem.getValue())) {// не в свою ветку

                if (draggedValue.getData().getParent() == null
                        || !draggedValue.getData().getParent().getId().equals(((BranchTreeNode) treeItem.getValue()).getData().getId())) {// не в свой уровень

                    treeModel.remove(draggedValue);
                    treeModel.add(treeItem.getValue(), new BranchTreeNode[]{draggedValue});

                    final boolean remove = draggedValue.getData().getParent() == null || draggedValue.getData().getParent().getChildren().remove(draggedValue.getData());
                    if (!remove) {
                        throw new SerialException("Not delete the child ");
                    }
                    draggedValue.getData().setParent(((BranchTreeNode) treeItem.getValue()).getData());
                    ((BranchTreeNode) treeItem.getValue()).getData().getChildren().add(draggedValue.getData());


                    repo.save(((BranchTreeNode) treeItem.getValue()).getData());
                    repo.save(draggedValue.getData());


                }
            }
        });

    }

}
