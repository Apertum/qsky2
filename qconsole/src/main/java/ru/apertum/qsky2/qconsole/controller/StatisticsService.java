/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsky2.qconsole.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;
import ru.apertum.qsky2.model.Step;
import ru.apertum.qsky2.qconsole.core.ConsoleRepo;
import ru.apertum.qsky2.qconsole.core.Record;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MINUTE;

/**
 * @author Evgeniy Egorov
 */
@Service("stat")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@RequiredArgsConstructor
public class StatisticsService {

    private final ConsoleRepo repo;

    public List<Step> loadStats(long branchId, Date start, Date finish) {
        final GregorianCalendar day = new GregorianCalendar();
        day.setTime(finish);
        day.set(HOUR_OF_DAY, 23);
        day.set(MINUTE, 59);
        return repo.getSteps(branchId, start, day.getTime());
    }

    public List<Record> prepareDataByUser(List<Step> steps) {
        final ArrayList<Record> recs = new ArrayList<>();
        for (Step step : steps) {
            boolean f = true;
            for (Record rec : recs) {
                if (rec.getBranchId().equals(step.getBranchId()) && rec.getServiceId().equals(step.getServiceId()) && rec.getEmployeeId().equals(step.getEmployeeId())) {
                    if (step.getFinishState() == 0) {
                        rec.oneMoreRemoved();
                    } else {
                        rec.setAvgMin(rec.getAvgMin() * rec.getServed() + (int) (step.getWorking() / 1000 / 60));
                        rec.oneMoreServed();
                        rec.setAvgMin(rec.getAvgMin() / rec.getServed());
                    }
                    f = false;
                    break;
                }
            }
            if (f) {
                recs.add(new Record(step.getBranchId(), step.getServiceId(), step.getEmployeeId(),
                        step.getFinishState() == null || step.getFinishState() == 0 ? 0 : 1,
                        step.getFinishState() == null || step.getFinishState() == 0 ? 1 : 0,
                        step.getFinishState() == null || step.getFinishState() == 0 ? 0 : ((int) (step.getWorking() / 1000 / 60)),
                        repo.getServiceName(step.getBranchId(), step.getServiceId()), repo.getEmployeeName(step.getBranchId(), step.getEmployeeId())));
            }
        }
        return recs;
    }
}
