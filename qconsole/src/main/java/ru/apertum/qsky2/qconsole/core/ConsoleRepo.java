/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsky2.qconsole.core;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.apertum.qsky2.model.Branch;
import ru.apertum.qsky2.model.Customer;
import ru.apertum.qsky2.model.Element;
import ru.apertum.qsky2.model.Employee;
import ru.apertum.qsky2.model.QSkySrv;
import ru.apertum.qsky2.model.Step;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.List;

/**
 * @author Evgeniy Egorov
 */
@Repository("repo")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@RequiredArgsConstructor
@Log4j2
public class ConsoleRepo {

    @PersistenceContext
    private final EntityManager em;

    private final User user;

    private List<QSkySrv> servs = null;
    private List<Employee> empls = null;
    private long time = 0;

    private void refreshDicts() {
        final long now = System.currentTimeMillis();
        if (now - time > 1000 * 5 || servs == null || empls == null) {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<Employee> query = builder.createQuery(Employee.class);
            query.from(Employee.class);
            TypedQuery<Employee> q = em.createQuery(query);
            empls = q.getResultList();

            CriteriaQuery<QSkySrv> querySrv = builder.createQuery(QSkySrv.class);
            querySrv.from(QSkySrv.class);
            TypedQuery<QSkySrv> qSrv = em.createQuery(querySrv);
            servs = qSrv.getResultList();

            time = now;
        }
    }

    public String getServiceName(Long branchId, Long serviceId) {
        refreshDicts();
        for (QSkySrv serv : servs) {
            if (serv.getBranchId().equals(branchId) && serv.getServiceId().equals(serviceId)) {
                return serv.getName();
            }
        }
        return serviceId == null ? "" : serviceId.toString();
    }

    public String getEmployeeName(Long branchId, Long employeeId) {
        refreshDicts();
        for (Employee empl : empls) {
            if (empl.getBranchId().equals(branchId) && empl.getEmployeeId().equals(employeeId)) {
                return empl.getName();
            }
        }
        return employeeId == null ? "" : employeeId.toString();
    }

    @Transactional
    public void delete(Object entity) {
        em.remove(em.contains(entity) ? entity : em.merge(entity));
    }

    @Transactional
    public void save(Object entity) {
        // тут что-то не понятное творится. Вылетает из контекста сущность и просерается ID автоинкрементно проставленное.
        Element obj = (Element) (em.contains(entity) ? entity : em.merge(entity));
        em.persist(obj);
        ((Element) entity).setId(obj.getId());
    }

    public List<Branch> getBranches() {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Branch> query = builder.createQuery(Branch.class);
        Root<Branch> root = query.from(Branch.class);
        query.select(root).orderBy(builder.desc(root.get("id")));
        if (!user.getBranches().isEmpty()) {
            query.select(root).where(builder.in(root.get("branchId").in(user.getBranches())));
        }
        TypedQuery<Branch> q = em.createQuery(query);
        return q.getResultList();
    }

    public Branch getBranch(Long branchId) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Branch> query = builder.createQuery(Branch.class);
        Root<Branch> root = query.from(Branch.class);
        query.select(root).orderBy(builder.desc(root.get("id")));
        if (user.getBranches().isEmpty()) {
            query.select(root).where(builder.equal(root.get("branchId"), branchId));
        } else {
            query.select(root).where(builder.and(builder.equal(root.get("branchId"), branchId), builder.in(root.get("branchId").in(user.getBranches()))));
        }
        TypedQuery<Branch> q = em.createQuery(query);
        return q.getResultList().isEmpty() ? null : q.getResultList().get(0);
    }

    public List<Step> getSteps(long branchId, Date start, Date finish) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Step> query = builder.createQuery(Step.class);
        Root<Step> root = query.from(Step.class);
        query.select(root).where(builder.and(builder.equal(root.get("branchId"), branchId),
                builder.isNotNull(root.get("finishTime")),
                builder.between(root.get("standTime"), start, finish)));
        TypedQuery<Step> q = em.createQuery(query);
        return q.getResultList();
    }

    public List<Customer> getCustomers(Long branchId, Date start, Date finish) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Customer> query = builder.createQuery(Customer.class);
        Root<Customer> root = query.from(Customer.class);
        query.select(root).where(builder.and(builder.equal(root.get("branchId"), branchId),
                builder.between(root.get("visitTime"), start, finish)));
        TypedQuery<Customer> q = em.createQuery(query);
        return q.getResultList();
    }
}
