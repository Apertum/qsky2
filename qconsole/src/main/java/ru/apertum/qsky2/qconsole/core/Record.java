package ru.apertum.qsky2.qconsole.core;

import lombok.Data;

@Data
public class Record {

    private final Long branchId;
    private final Long serviceId;
    private final Long employeeId;
    private final String serviceName;
    private final String employeeName;
    private int served;
    private int removed;
    private int avgMin;

    public Record(Long branchId, Long serviceId, Long employeeId, int served, int removed, int avgMin, String serviceName, String employeeName) {
        this.branchId = branchId;
        this.serviceId = serviceId;
        this.employeeId = employeeId;
        this.served = served;
        this.removed = removed;
        this.avgMin = avgMin;
        this.serviceName = serviceName;
        this.employeeName = employeeName;
    }

    public void oneMoreServed() {
        served++;
    }

    public void oneMoreRemoved() {
        removed++;
    }

    @Override
    public String toString() {
        return String.format("{br=%d, srv=%d, emp=%d}", branchId, serviceId, employeeId);
    }

}
