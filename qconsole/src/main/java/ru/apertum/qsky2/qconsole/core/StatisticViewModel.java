/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsky2.qconsole.core;

import lombok.Data;
import org.zkoss.util.resource.Labels;
import org.zkoss.zul.GroupComparator;
import org.zkoss.zul.GroupsModelArray;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * @author Evgeniy Egorov
 */
@Data
public class StatisticViewModel {
    public boolean showGroup = true;
    private Date start;
    private Date finish;
    private int regim;
    private GroupsModelArray statisticModel = new StaticticGroupingEmplsModel(new ArrayList<>(), new RecordEmployeeComparator());

    public static class StaticticGroupingServsModel extends GroupsModelArray<Record, String, String, Object> {

        public StaticticGroupingServsModel(List<Record> data, Comparator<Record> cmpr) {
            super(data.toArray(new Record[data.size()]), cmpr);
        }

        public String l(String resName) {
            return Labels.getLabel(resName);
        }

        @Override
        protected String createGroupHead(Record[] groupdata, int index, int col) {
            int served = 0;
            int removed = 0;
            int avgMin = 0;
            for (Record rec : groupdata) {
                served = served + rec.getServed();
                removed = removed + rec.getRemoved();
                avgMin = avgMin + rec.getAvgMin();
            }
            return groupdata.length == 0 ? l("empty_group")
                    : l("service") + " " + "\"" + (groupdata[0].getServiceName().length() > 30 ? groupdata[0].getServiceName().substring(0, 30) + "..." : groupdata[0].getServiceName()) + "\""
                    + " " + l("served2") + " " + served
                    + ", " + l("killed2") + " " + removed
                    + ", " + l("avd_working") + " " + (avgMin / groupdata.length);
        }

        @Override
        protected String createGroupFoot(Record[] groupdata, int index, int col) {
            return String.format(l("amount_users_for_1service") + ": %d", groupdata.length);
        }

        @Override
        public boolean hasGroupfoot(int groupIndex) {
            return true;
        }
    }

    public static class StaticticGroupingEmplsModel extends GroupsModelArray<Record, String, String, Object> {

        public StaticticGroupingEmplsModel(List<Record> data, Comparator<Record> cmpr) {
            super(data.toArray(new Record[data.size()]), cmpr);
        }

        public String l(String resName) {
            return Labels.getLabel(resName);
        }

        @Override
        protected String createGroupHead(Record[] groupdata, int index, int col) {
            int served = 0;
            int removed = 0;
            int avgMin = 0;
            for (Record rec : groupdata) {
                served = served + rec.getServed();
                removed = removed + rec.getRemoved();
                avgMin = avgMin + rec.getAvgMin();
            }
            return groupdata.length == 0 ? l("empty_group")
                    : l("employee") + " " + "\"" + groupdata[0].getEmployeeName() + "\""
                    + " " + l("served2") + " " + served
                    + ", " + l("killed2") + " " + removed
                    + ", " + l("avd_working") + " " + (avgMin / groupdata.length);
        }

        @Override
        protected String createGroupFoot(Record[] groupdata, int index, int col) {
            return String.format(l("served_in_amount") + ": %d", groupdata.length);
        }

        @Override
        public boolean hasGroupfoot(int groupIndex) {
            return true;
        }
    }

    public static class RecordEmployeeComparator implements Comparator<Record>, GroupComparator<Record>, Serializable {

        @Override
        public int compare(Record o1, Record o2) {
            return o1.getEmployeeId().compareTo(o2.getEmployeeId());
        }

        @Override
        public int compareGroup(Record o1, Record o2) {
            if (o1.getEmployeeId() == null && o2.getEmployeeId() == null) {
                return 0;
            }
            if (o1.getEmployeeId() != null && o2.getEmployeeId() == null) {
                return 1;
            }
            if (o1.getEmployeeId() == null && o2.getEmployeeId() != null) {
                return -1;
            }
            return o1.getEmployeeId().compareTo(o2.getEmployeeId());
        }
    }

    public static class RecordServiceComparator implements Comparator<Record>, GroupComparator<Record>, Serializable {

        @Override
        public int compare(Record o1, Record o2) {
            return o1.getServiceId().compareTo(o2.getServiceId());
        }

        @Override
        public int compareGroup(Record o1, Record o2) {
            if (o1.getServiceId() == null && o2.getServiceId() == null) {
                return 0;
            }
            if (o1.getServiceId() != null && o2.getServiceId() == null) {
                return 1;
            }
            if (o1.getServiceId() == null && o2.getServiceId() != null) {
                return -1;
            }
            return o1.getServiceId().compareTo(o2.getServiceId());
        }
    }

}
