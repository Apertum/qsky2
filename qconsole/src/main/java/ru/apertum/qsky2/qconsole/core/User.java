package ru.apertum.qsky2.qconsole.core;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import ru.apertum.qsky2.model.Branch;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component("user")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class User implements Serializable {

    @Value("${qconsole.users}")
    @Getter
    private String qconsoleUsers;
    private Map<String, Pair<String, List<Long>>> names = new HashMap();
    @Getter
    @Setter
    private String name = "demo";
    @Getter
    @Setter
    private String password = "demo";

    @Getter
    @Setter
    private Branch currentBranch;

    @Getter
    private Set<Long> branches = new HashSet<>();

    public void setQconsoleUsers(String qconsoleUsers) {
        this.qconsoleUsers = qconsoleUsers;
        init();
    }

    @PostConstruct
    private void init() {
        names.clear();
        String[] ss = qconsoleUsers.toLowerCase().replaceAll("\\s+", "").split("[;,]");
        for (String s : ss) {
            final String[] ss1 = s.split("=");
            if (ss1.length != 2) {
                continue;
            }
            final String[] ss2 = ss1[1].split("[@$&%#]");
            final List<Long> branches = new ArrayList<>();
            if (ss2.length == 2) {
                final String[] ss3 = ss2[1].split("[._-]");
                for (String ss31 : ss3) {
                    if (ss31.matches("\\d+")) {
                        branches.add(Long.parseLong(ss31));
                    }
                }
            }
            names.put(ss1[0], Pair.of(ss2[0], branches));
        }
    }

    public boolean permission(Long branchId) {
        return branches.isEmpty() || branches.contains(branchId);
    }

    public boolean validate() {
        return validate(name, password);
    }

    public boolean validate(@NonNull String name, @NonNull String pass) {
        if (names.get(name) != null && pass.equals(names.get(name).getLeft())) {
            branches.addAll(names.get(name).getRight());
            return true;
        } else {
            return false;
        }
    }
}
