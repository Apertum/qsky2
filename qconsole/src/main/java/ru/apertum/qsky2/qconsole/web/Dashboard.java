/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsky2.qconsole.web;

import lombok.Data;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Footer;
import org.zkoss.zul.Grid;
import org.zkoss.zul.GroupsModelArray;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Panel;
import org.zkoss.zul.PieModel;
import org.zkoss.zul.SimplePieModel;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Window;
import org.zkoss.zul.event.TreeDataEvent;
import ru.apertum.qsky2.model.Branch;
import ru.apertum.qsky2.model.Customer;
import ru.apertum.qsky2.model.Step;
import ru.apertum.qsky2.qconsole.controller.BranchTreeRenderer;
import ru.apertum.qsky2.qconsole.controller.StatisticsService;
import ru.apertum.qsky2.qconsole.controller.branch_tree.AdvancedTreeModel;
import ru.apertum.qsky2.qconsole.controller.branch_tree.BranchTreeNode;
import ru.apertum.qsky2.qconsole.core.ConsoleRepo;
import ru.apertum.qsky2.qconsole.core.Multilingual;
import ru.apertum.qsky2.qconsole.core.StatisticViewModel;
import ru.apertum.qsky2.qconsole.core.User;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * @author Evgeniy Egorov
 */
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
@Log4j2
public class Dashboard {

    @WireVariable("repo")
    private ConsoleRepo repo;

    @WireVariable("user")
    private User user;

    @WireVariable("tree")
    private AdvancedTreeModel treeModel;

    @Getter
    @WireVariable("stat")
    private StatisticsService statService;

    @WireVariable("renderer")
    private BranchTreeRenderer treeRenderer;

    private static final long serialVersionUID = 3814570327995355261L;

    @Getter
    private final ArrayList<Pair> servicesCustList = new ArrayList();
    @Getter
    private final PieChartVM pieChart = new PieChartVM();
    @Getter
    private final DialChartVM dialChart = new DialChartVM();
    @Getter
    private final StatisticViewModel statVM = new StatisticViewModel();

    // *** Диалоги изменения состояния
    @Wire("#incBranchPropsDialog #branchPropsDialog")
    Window branchPropsDialog;

    private Multilingual.Lng lang = new Multilingual().init();

    @Wire
    private Window dashboardWindow;
    @Wire
    private Tree tree;
    //**** Multilingual
    ////////////**************************************************
    ////////////**************************************************
    ////////////**************************************************

    @Wire
    private Label branchName;
    @Wire
    private Panel statPapamPanel;
    private Branch selectedBranch = null;
    @Wire
    private Grid statisticGrid;
    @Wire
    private Footer footer_category;

    public String l(String resName) {
        return Labels.getLabel(resName);
    }

    @Init
    public void init() {

    }

    @Command
    public void about() {
        final Properties settings = new Properties();
        final InputStream inStream = this.getClass().getResourceAsStream("/qskyapi.properties");
        try {
            settings.load(inStream);
        } catch (IOException ex) {
            throw new RuntimeException("Cant read version. " + ex);
        }
        Messagebox.show("***  QSkyAPI  ***\n"
                        + "   version " + settings.getProperty("version")
                        + "\n   date " + settings.getProperty("date")
                        + "\n   for QSkySenderPlugin version=" + settings.getProperty("support_clients"),
                "QMS Apertum-QSystem", Messagebox.OK, Messagebox.INFORMATION);
    }

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
        Selectors.wireEventListeners(view, this);
        doAfterCompose(null);
    }

    //*****************************************************
    //**** Multilingual
    //*****************************************************
    public ArrayList<Multilingual.Lng> getLangs() {
        return Multilingual.LANGS;
    }

    public Multilingual.Lng getLang() {
        return lang;
    }

    public void setLang(Multilingual.Lng lang) {
        this.lang = lang;
    }

    @Command("changeLang")
    public void changeLang() {
        if (lang != null) {
            final org.zkoss.zk.ui.Session session = Sessions.getCurrent();
            final Locale prefer_locale = lang.code.length() > 2
                    ? new Locale(lang.code.substring(0, 2), lang.code.substring(3)) : new Locale(lang.code);
            session.setAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE, prefer_locale);
            Executions.sendRedirect(null);
        }
    }

    public void doAfterCompose(Component comp) {
        treeRenderer.setBranchPropsDialog(branchPropsDialog);
        tree.setItemRenderer(treeRenderer);
        tree.setModel(treeModel);
        branchName.setValue("Not selected");
    }

    @Command
    public void addBranch() {
        if (tree.getSelectedItem() == null && !user.getBranches().isEmpty()) {
            return;
        }
        final Long l = System.currentTimeMillis() % 10000;
        tree.setSelectedItem(null);
        // dialog
        final Window w = branchPropsDialog;
        w.setTitle("New branch");
        ((Textbox) w.getFellow("nameBranch")).setText("New branch " + l);
        ((Intbox) w.getFellow("idBranch")).setValue(l.intValue());
        ((Intbox) w.getFellow("timeZone")).setValue(0);
        w.setVisible(true);
        w.setPosition("center");
        w.doModal();
    }

    @Command
    public void removeBranch() {
        if (tree.getSelectedItem() != null) {
            if (((BranchTreeNode) tree.getSelectedItem().getValue()).getParent().getData() == null && !user.getBranches().isEmpty()) {
                return;
            }
            if (((BranchTreeNode) tree.getSelectedItem().getValue()).getChildCount() != 0) {
                Messagebox.show(l("mess_del_branch_not_empty_tree"), l("del_impossible"), Messagebox.OK, Messagebox.ERROR);
                return;
            }
            final Branch br = ((BranchTreeNode) tree.getSelectedItem().getValue()).getData();
            Messagebox.show(l("shure_del_branch") + " \"" + br + "\"?", l("removing"), Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, (Event evt) -> {
                if (evt.getName().equals("onOK")) {

                    if (br.getParent() != null && !br.getParent().getChildren().remove(br)) {
                        throw new RuntimeException("Not delete the child ");
                    }

                    repo.delete(br);

                    ((BranchTreeNode) tree.getSelectedItem().getValue()).getParent().remove(tree.getSelectedItem().getValue());
                }
            });
        }
    }

    @Command
    public void closeBranchPropsDialog() {
        final Branch br;
        final BranchTreeNode br_node;
        if (tree.getSelectedItem() != null) {
            br_node = tree.getSelectedItem().getValue();
            br = ((BranchTreeNode) tree.getSelectedItem().getValue()).getData();
        } else {
            br_node = null;
            br = new Branch();
        }
        br.setName(((Textbox) branchPropsDialog.getFellow("nameBranch")).getText());
        br.setBranchId(((Intbox) branchPropsDialog.getFellow("idBranch")).getValue().longValue());
        br.setTimeZone(((Intbox) branchPropsDialog.getFellow("timeZone")).getValue());
        repo.save(br);
        if (br_node == null) {
            if (user.getBranches().isEmpty()) {
                ((BranchTreeNode) tree.getModel().getRoot()).insert(new BranchTreeNode(br), 0);
            } else {
                br.setParent(((BranchTreeNode) tree.getSelectedItem().getValue()).getData());
                ((BranchTreeNode) tree.getSelectedItem().getValue()).getData().getChildren().add(br);
                final BranchTreeNode node = new BranchTreeNode(br);
                ((BranchTreeNode) tree.getSelectedItem().getValue()).add(node);
            }
        } else {
            final int[] ii1 = treeModel.getPath(br_node);
            final int[] ii2 = (new int[ii1.length - 1]);
            System.arraycopy(ii1, 0, ii2, 0, ii2.length);
            treeModel.fireEvent(TreeDataEvent.CONTENTS_CHANGED, ii2, tree.getSelectedItem().getIndex(), tree.getSelectedItem().getIndex());
        }

        branchPropsDialog.setVisible(false);
    }

    @Listen("onSelect = #tree")
    @NotifyChange(value = {"branchName", "statPapamPanel"})
    public void selectBranch() {
        BranchTreeNode selectedNode = tree.getSelectedItem().getValue();
        log.info("Branch '{}' was selected ", selectedNode.getData());
        user.setCurrentBranch(selectedNode.getData());
        selectedBranch = selectedNode.getData();
        branchName.setValue(selectedNode.getData().getName());
        statPapamPanel.setTitle(selectedNode.getData().getName());
    }

    @Command("showBranchSituation")
    @NotifyChange(value = {"pieChart", "dialChart", "servicesCustList"})
    public synchronized void showBranchSituation() {
        if (selectedBranch == null) {
            return;
        }
        final GregorianCalendar day = new GregorianCalendar();
        day.set(GregorianCalendar.HOUR_OF_DAY, 0);
        day.set(GregorianCalendar.MINUTE, 0);
        final Date today_m = day.getTime();
        day.set(GregorianCalendar.HOUR_OF_DAY, 23);
        day.set(GregorianCalendar.MINUTE, 59);
        final List<Customer> custs = repo.getCustomers(selectedBranch.getBranchId(), today_m, day.getTime());


        final Predicate<? super Customer> filter = (cust) -> {
            return cust.getState() != 0 && cust.getState() != 10;
        };

        final HashMap<Long, Integer> cnt = new HashMap<>();
        Stream<Customer> scu = custs.stream().filter(filter);

        final long clntsCnt = custs.stream().filter(filter).count();
        scu.forEach((cust) -> {
            Integer serv = cnt.get(cust.getServiceId());
            if (serv != null) {
                cnt.put(cust.getServiceId(), ++serv);
            } else {
                cnt.put(cust.getServiceId(), 1);
            }
        });
        scu = custs.stream().filter(filter);

        final Optional<Customer> mw = scu.max((cust1, cust2) -> {
            return (cust1.getWaiting() == 0 ? (new Long(new Date().getTime() - cust1.getVisitTime().getTime())) : cust1.getWaiting()).compareTo(cust2.getWaiting() == 0 ? (new Date().getTime() - cust2.getVisitTime().getTime()) : cust2.getWaiting());
        });
        long maxWaiting = mw.isPresent() ? (mw.get().getWaiting() == 0 ? (new Date().getTime() - mw.get().getVisitTime().getTime()) : mw.get().getWaiting()) : 0;

        int i = 0;
        long avg = 0;
        for (Customer cust : custs) {
            if (cust.getState() != 0 && cust.getWaiting() != null) {
                avg = avg + (cust.getWaiting() == 0 ? (new Date().getTime() - cust.getVisitTime().getTime()) : cust.getWaiting());
                i++;
            }
        }

        final List<Pair> ls = new ArrayList<>();
        cnt.keySet().stream().forEach((l) -> {
            ls.add(new Pair(l, cnt.get(l)));
        });
        final PieModel model = new SimplePieModel();
        ls.sort((Pair o1, Pair o2) -> {
            return Integer.compare(o2.i, o1.i);
        });
        ls.stream().limit(10).forEach((Pair p) -> {
            final String name = repo.getServiceName(selectedBranch.getBranchId(), p.l);
            model.setValue((name.length() > 30 ? name.substring(0, 30) + "..." : name) + "(" + p.i + ")", p.i);
        });
        servicesCustList.clear();
        ls.stream().forEach((Pair p) -> {
            final String name = repo.getServiceName(selectedBranch.getBranchId(), p.l);
            servicesCustList.add(new Pair(name, p.i));
        });
        pieChart.setModel(model);

        dialChart.getAverageModel().setValue(0, i == 0 ? 0 : avg / i / 1000 / 60);
        dialChart.getCustomersModel().setValue(0, clntsCnt);
        dialChart.getWaitingModel().setValue(0, maxWaiting < 1000 * 60 ? (clntsCnt == 0 ? 0 : 1) : maxWaiting / 1000 / 60);
    }

    @Command("showBranchStatistic")
    @NotifyChange(value = {"statVM", "statisticGrid", "footer_category"})
    public synchronized void showBranchStatistic() {
        if (selectedBranch == null) {
            return;
        }
        final List<Step> stst = statService.loadStats(selectedBranch.getBranchId(), statVM.getStart(), statVM.getFinish());

        GroupsModelArray mo = statVM.getRegim() == 0
                ? new StatisticViewModel.StaticticGroupingEmplsModel(statService.prepareDataByUser(stst), new StatisticViewModel.RecordEmployeeComparator())
                : new StatisticViewModel.StaticticGroupingServsModel(statService.prepareDataByUser(stst), new StatisticViewModel.RecordServiceComparator());
        statVM.setStatisticModel(mo);
        statisticGrid.setModel(mo);
        footer_category.setLabel((statVM.getRegim() == 0 ? l("amount_users") + " : " : l("amount_services") + " : ") + mo.getGroupCount());
    }

    @Command("downloadBranchStatistic")
    public synchronized void downloadBranchStatistic() {
        if (selectedBranch == null) {
            return;
        }
        final GregorianCalendar day = new GregorianCalendar();
        day.setTime(statVM.getStart());
        day.set(GregorianCalendar.HOUR_OF_DAY, 0);
        day.set(GregorianCalendar.MINUTE, 0);
        final Date today_m = day.getTime();
        day.setTime(statVM.getFinish());
        day.set(GregorianCalendar.HOUR_OF_DAY, 23);
        day.set(GregorianCalendar.MINUTE, 59);
        final List<Customer> custs = repo.getCustomers(selectedBranch.getBranchId(), today_m, day.getTime());
        final StringBuffer sb;
        sb = new StringBuffer(l("captions_csv") + "\n");
        int nom = 0;
        for (Customer cust : custs) {
            Step step = cust.getFirstStep();
            while (step != null) {
                sb.append(++nom).append(";");
                sb.append(selectedBranch.getName()).append(";");
                sb.append(repo.getServiceName(selectedBranch.getBranchId(), step.getServiceId())).append(";");
                sb.append(repo.getEmployeeName(selectedBranch.getBranchId(), step.getEmployeeId())).append(";");
                sb.append(cust.getPrefix()).append(cust.getNumber()).append(";");
                sb.append(SimpleDateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(step.getStandTime())).append(";");
                if (step.getFinishState() == null || step.getFinishState() == 0) {
                    sb.append(";;;;");
                } else {
                    sb.append(SimpleDateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(step.getStartTime())).append(";");
                    sb.append(SimpleDateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(step.getFinishTime())).append(";");
                    sb.append(step.getWaiting() / 1000 / 60).append(";");
                    sb.append(step.getWorking() / 1000 / 60).append(";");
                }
                sb.append(step.getStartState()).append(";");
                sb.append(step.getFinishState() == null ? "" : step.getFinishState()).append(";");
                step = step.getAfter();
                sb.append("\n");
            }
        }
        Filedownload.save(sb.toString().getBytes(), "text/csv", "qstat_" + SimpleDateFormat.getDateInstance().format(statVM.getStart()) + "-" + SimpleDateFormat.getDateInstance().format(statVM.getFinish()) + ".csv");
        sb.setLength(0);
    }

    @Data
    public static class Pair implements Comparable<Pair> {

        Long l;
        Integer i;
        String st;

        public Pair(Long l, Integer i) {
            this.l = l;
            this.i = i;
        }

        public Pair(String st, Integer i) {
            this.st = st;
            this.i = i;
        }

        @Override
        public int compareTo(Pair o) {
            return i.compareTo(o.i);
        }
    }
}
