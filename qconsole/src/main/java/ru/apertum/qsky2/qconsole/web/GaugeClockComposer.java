/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsky2.qconsole.web;

import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import ru.apertum.qsky2.qconsole.core.User;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Evgeniy Egorov
 */
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class GaugeClockComposer extends SelectorComposer<Div> {

    private static final String DATE_FORMAT = "HH:mm:ss";

    @WireVariable("user")
    private User user;

    /**
     * Формат даты.
     */
    private static final  DateFormat FORMAT_D = new SimpleDateFormat(DATE_FORMAT);

    @Wire
    Label timeInBranch;

    private long lastRead = 0;
    private int dx = 0;

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
        Selectors.wireEventListeners(view, this);
    }

    @Listen("onTimer = #timer")
    public void updateData() {
        final Date date = new Date();
        if (date.getTime() - lastRead > 3000) {
            lastRead = date.getTime();
            dx = user.getCurrentBranch() == null ? 0 : user.getCurrentBranch().getTimeZone();
        }
        date.setTime(date.getTime() + dx * 1000 * 60);
        timeInBranch.setValue(FORMAT_D.format(date));
    }

}
