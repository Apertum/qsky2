package ru.apertum.qsky2.qconsole.web;

import lombok.Getter;
import lombok.Setter;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import ru.apertum.qsky2.qconsole.core.Multilingual;
import ru.apertum.qsky2.qconsole.core.User;

import java.util.ArrayList;
import java.util.Locale;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class UserLoginForm {

    @WireVariable("user")
    private User user;

    @Getter
    @Setter
    private User userForm = new User();

    private Multilingual.Lng lang = new Multilingual().init();

    @Init
    public void init() {
        userForm.setQconsoleUsers(user.getQconsoleUsers());
        Sessions.getCurrent().setAttribute("USER", userForm);
    }

    public ArrayList<Multilingual.Lng> getLangs() {
        return Multilingual.LANGS;
    }

    public Multilingual.Lng getLang() {
        return lang;
    }

    public void setLang(Multilingual.Lng lang) {
        this.lang = lang;
    }
    //**** Multilingual
    ////////////**************************************************
    ////////////**************************************************
    ////////////**************************************************

    @Command("changeLang")
    public void changeLang() {
        if (lang != null) {
            final org.zkoss.zk.ui.Session session = Sessions.getCurrent();
            final Locale prefer_locale = lang.code.length() > 2
                    ? new Locale(lang.code.substring(0, 2), lang.code.substring(3)) : new Locale(lang.code);
            session.setAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE, prefer_locale);
            Executions.sendRedirect(null);
        }
    }

    @Command
    public void submit() {
        user.setName(userForm.getName());
        user.setPassword(userForm.getPassword());
        user.validate();
        Executions.sendRedirect("~./dashboard.zul");
    }
}
