package ru.apertum.qsky2.qconsole.web;

import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Sessions;
import ru.apertum.qsky2.qconsole.core.User;

public class UserLoginValidator extends AbstractValidator {

    @Override
    public void validate(ValidationContext ctx) {
        final String name = (String) ctx.getProperties("name")[0].getValue();
        final String password = (String) ctx.getProperties("password")[0].getValue();
        final User user = (User) Sessions.getCurrent().getAttribute("USER");
        if (user == null || !user.validate(name, password)) {
            this.addInvalidMessage(ctx, "login", Labels.getLabel("access_denied"));
        }
    }
}
