package ru.apertum.qsky2.pager;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Answer {

    @JsonProperty("curr_version")
    private String currVersion;

    private String urlfun;

    private List<PagerData> data;
}
