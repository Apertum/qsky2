package ru.apertum.qsky2.pager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class Config {

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource getQskyDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    public JdbcTemplate getQskyJdbcTemplate(@Autowired DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}
