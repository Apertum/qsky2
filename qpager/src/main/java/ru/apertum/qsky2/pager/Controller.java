package ru.apertum.qsky2.pager;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import javax.servlet.http.HttpServletRequest;

@RequiredArgsConstructor
@RestController
@RequestMapping("/qskyapi")
public class Controller {

    private final HttpServletRequest request;

    private final PagerService service;

    @GetMapping(value = "getpagerdata", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<Answer>> get(@RequestParam @Nullable String qsysver,
                                            @RequestParam @Nullable String checkdb,
                                            @RequestParam @Nullable String checkdb2,
                                            @RequestParam @Nullable String qplugins) {
        final Mono<Answer> objectMono = Mono.fromCallable(() -> service.getData(qsysver, checkdb, checkdb2, qplugins, request.getRemoteAddr()));
        return objectMono.map(ResponseEntity::ok).defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping(value = "setpagerdata")
    public void set(@RequestParam @Nullable String qsysver,
                    @RequestParam @Nullable String dataid,
                    @RequestParam @Nullable String inputdata,
                    @RequestParam @Nullable String qplugins,
                    @RequestParam @Nullable String quizid) {
        service.setData(qsysver, dataid, inputdata, qplugins, quizid, request.getRemoteAddr());
    }
}
