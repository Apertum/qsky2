package ru.apertum.qsky2.pager;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Структура сообщения для пейджера.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "pager_data")
@NamedQueries({
        @NamedQuery(name = "PagerData.findAll", query = "SELECT p FROM PagerData p"),
        @NamedQuery(name = "PagerData.findById", query = "SELECT p FROM PagerData p WHERE p.id = :id"),
        @NamedQuery(name = "PagerData.findByDataType", query = "SELECT p FROM PagerData p WHERE p.dataType = :dataType"),
        @NamedQuery(name = "PagerData.findByTextData", query = "SELECT p FROM PagerData p WHERE p.textData = :textData"),
        @NamedQuery(name = "PagerData.findByQuizCaption", query = "SELECT p FROM PagerData p WHERE p.quizCaption = :quizCaption"),
        @NamedQuery(name = "PagerData.findByStartDate", query = "SELECT p FROM PagerData p WHERE p.startDate = :startDate"),
        @NamedQuery(name = "PagerData.findByActive", query = "SELECT p FROM PagerData p WHERE p.active = :active")})
public class PagerData implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    @JsonProperty("id")
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "data_type")
    @JsonProperty("type")
    private int dataType;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1450)
    @Column(name = "text_data")
    @JsonProperty("text")
    private String textData;

    @Size(max = 145)
    @Column(name = "quiz_caption")
    @JsonProperty("qcap")
    private String quizCaption;

    @Basic(optional = false)
    @NotNull
    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Basic(optional = false)
    @NotNull
    @Column(name = "active")
    private boolean active;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pagerDataId", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<PagerResults> pagerResultsList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pagerDataId", fetch = FetchType.EAGER)
    @JsonProperty("quis_items")
    private List<PagerQuizItems> pagerQuizItemsList;

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PagerData)) {
            return false;
        }
        PagerData other = (PagerData) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.apertum.qsky.model.pager.PagerData[ id=" + id + " ]";
    }
}
