package ru.apertum.qsky2.pager;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 * Элемент для выбора в пейджере на вопрос.
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "pager_quiz_items")
@NamedQueries({
        @NamedQuery(name = "PagerQuizItems.findAll", query = "SELECT p FROM PagerQuizItems p"),
        @NamedQuery(name = "PagerQuizItems.findById", query = "SELECT p FROM PagerQuizItems p WHERE p.id = :id"),
        @NamedQuery(name = "PagerQuizItems.findByItemText", query = "SELECT p FROM PagerQuizItems p WHERE p.itemText = :itemText")})
public class PagerQuizItems implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    @JsonProperty("id")
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 145)
    @Column(name = "item_text")
    @JsonProperty("text")
    private String itemText;

    @OneToMany(mappedBy = "quizId", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<PagerResults> pagerResultsList;

    @JoinColumn(name = "pager_data_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JsonIgnore
    private PagerData pagerDataId;

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PagerQuizItems)) {
            return false;
        }
        PagerQuizItems other = (PagerQuizItems) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ru.apertum.qsky.model.pager.PagerQuizItems[ id=" + id + " ]";
    }
}
