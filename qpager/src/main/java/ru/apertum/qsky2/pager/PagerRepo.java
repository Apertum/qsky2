package ru.apertum.qsky2.pager;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@RequiredArgsConstructor
@Log4j2
public class PagerRepo {

    private final JdbcTemplate dao;

    @PersistenceContext
    private final EntityManager em;

    public List<PagerData> getPagerDataByActive(boolean isActive) {
        final Query query = em.createNamedQuery("PagerData.findByActive");
        query.setParameter("active", isActive);
        return query.getResultList();
    }

    public List<PagerData> getPagerDataById(Long id) {
        final Query query = em.createNamedQuery("PagerData.findById");
        query.setParameter("id", id);
        return query.getResultList();
    }

    @Transactional
    public void save(PagerResults pagerResults) {
        em.persist(pagerResults);
    }
}
