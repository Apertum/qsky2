package ru.apertum.qsky2.pager;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.net.BCodec;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static java.util.Calendar.MINUTE;

@Service
@RequiredArgsConstructor
@Log4j2
public class PagerService {

    @Value("${qsky.time-shift}")
    private Integer timeShift;

    @Value("${qsky.qsys-ver}")
    private String qsysVer;


    @Value("${qsky.qsky-url-fun}")
    private String qskyUrlFun;

    @Value("${qsky.ignor-addr}")
    private String black;

    private final PagerRepo repo;

    private final PagerAlreadyDone pagerAlreadyDone;

    public Answer getData(String qsysver, String checkdb, String checkdb2, String qplugins, String remoteAddr) {
        log.trace("GetPagerData in action.");
        if (notInBlackList(remoteAddr)) {
            if (checkdb2 == null) {
                checkdb2 = "no";
            } else {
                try {
                    checkdb2 = URLDecoder.decode(new BCodec().decode(checkdb2), StandardCharsets.UTF_8.name());
                } catch (Exception e) {
                    log.error(e);
                    try {
                        checkdb2 = URLDecoder.decode(checkdb2, StandardCharsets.UTF_8.name());
                    } catch (UnsupportedEncodingException ex) {
                        log.error(e);
                    }
                }
            }
            int cdb = -1;
            int usrs = 0;
            int srvs = 0;
            if (checkdb != null) {
                try {
                    final String[] ss = checkdb.split("-");
                    cdb = ss.length > 0 ? Integer.parseInt(ss[0]) : -1;
                    usrs = ss.length > 1 ? Integer.parseInt(ss[1]) : 0;
                    srvs = ss.length > 2 ? Integer.parseInt(ss[2]) : 0;
                } catch (NumberFormatException ex) {
                    log.error("some error.", ex);
                }
            }
            final Pair<String, String> macAndToken = macAndToken(qplugins);
            final PagerResults pagerResults = new PagerResults(remoteAddr, getNow(), qsysver, macAndToken.getFirst(), macAndToken.getSecond(), cdb, usrs, srvs, checkdb2);
            repo.save(pagerResults);
        }

        final List<PagerData> data = repo.getPagerDataByActive(true);
        final ArrayList<PagerData> forDell = new ArrayList<>();
        for (PagerData pagerData : data) {
            if (pagerData.getDataType() != 0 && pagerAlreadyDone.check(remoteAddr, pagerData.getId())) {
                forDell.add(pagerData);
            }
        }
        data.removeAll(forDell);
        return new Answer(qsysVer, qskyUrlFun, data);
    }

    public void setData(String qsysver, String dataid, String inputdata, String qplugins, String quizid, String remoteAddr) {
        final Pair<String, String> macAndToken = macAndToken(qplugins);
        final PagerResults pagerResults = new PagerResults(remoteAddr, getNow(), qsysver, macAndToken.getFirst(), macAndToken.getSecond(), 0, 0, 0, "");
        pagerAlreadyDone.add(remoteAddr, Long.parseLong(dataid));
        try {
            if (inputdata != null) {
                pagerResults.setInputData(URLDecoder.decode(new BCodec().decode(inputdata), StandardCharsets.UTF_8.name()));
            }
        } catch (DecoderException | UnsupportedEncodingException ex) {
            log.error(ex);
            try {
                pagerResults.setInputData(URLDecoder.decode(inputdata, StandardCharsets.UTF_8.name()));
            } catch (UnsupportedEncodingException e) {
                log.error(ex);
                pagerResults.setInputData(inputdata);
            }
        }

        final List<PagerData> data = repo.getPagerDataById(Long.parseLong(dataid));
        if (data.size() == 1) {
            PagerData pd = data.get(0);
            pagerResults.setPagerDataId(data.get(0));
            if (quizid != null) {
                long qid = Long.parseLong(quizid);
                for (PagerQuizItems qitem : pd.getPagerQuizItemsList()) {
                    if (qitem.getId() == qid) {
                        pagerResults.setQuizId(qitem);
                        break;
                    }
                }
            }
        }
        repo.save(pagerResults);
    }

    private boolean notInBlackList(@NonNull String remoteAddr) {
        for (String bb : black.split(";")) {
            if (remoteAddr.startsWith(bb)) {
                return false;
            }
        }
        return true;
    }

    private Date getNow() {
        final GregorianCalendar gc = new GregorianCalendar();
        gc.add(MINUTE, timeShift);
        return gc.getTime();
    }

    /**
     * Определим что пришло в параметрах.
     *
     * @param str значение из параметра.
     * @return пара: Mac - Token.
     */
    private Pair<String, String> macAndToken(String str) {
        if (str != null) {
            String[] pls = str.split("-");
            if (pls.length == 2) {
                return Pair.of(pls[0], pls[1]);
            } else {
                return Pair.of("err", str.length() < 45 ? str : (str.substring(0, 40) + "..."));
            }
        }
        return Pair.of("err", "err");
    }
}

