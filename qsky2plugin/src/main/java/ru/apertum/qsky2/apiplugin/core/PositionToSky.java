/*
 *  Copyright (C) 2010 {Apertum}Projects. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.apertum.qsky2.apiplugin.core;

import lombok.Data;
import ru.apertum.qsystem.common.model.QCustomer;
import ru.apertum.qsystem.server.model.QService;

/**
 * @author egorov
 */
@Data
public class PositionToSky {

    public enum Mode {
        INSERT, REMOVE
    }

    private final Mode mode;
    private final QCustomer customer;
    private final QService service;
    private final QCustomer before;
    private final QCustomer after;

    public PositionToSky(Mode mode, QCustomer customer, QCustomer before, QCustomer after) {
        this.mode = mode;
        this.customer = customer;
        this.service = customer.getService();
        this.before = before;
        this.after = after;
    }
}
