/*
 *  Copyright (C) 2010 {Apertum}Projects. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.apertum.qsky2.apiplugin.events;

import lombok.extern.log4j.Log4j2;
import ru.apertum.qsky2.apiplugin.IQSkyPluginUID;
import ru.apertum.qsky2.apiplugin.ws.QskyAuth;
import ru.apertum.qsky2.apiplugin.ws.SkyService;
import ru.apertum.qsystem.extra.IDataExchange;
import ru.apertum.qsystem.server.ServerProps;

/**
 * @author egorov
 */
@Log4j2
public class DataExchange implements IDataExchange, IQSkyPluginUID {

    @Override
    public void sendServiceName(Long branchId, Long serviceId, String name) {
        if (!QskyAuth.getInstance().isReady(ServerProps.getInstance().getProperty("QSKY", "token", "dev"))) {
            log.error("Plugin version of \"QSky2SenderPlugin\" not supported this version of QSystem cloud.");
            return;
        }
        log.debug("Service to Sky: " + name + "(" + serviceId + "/" + branchId + ")");
        SkyService.getInstance().sendServiceName(branchId, serviceId, name);
    }

    @Override
    public void sendUserName(Long branchId, Long userId, String name) {
        if (!QskyAuth.getInstance().isReady(ServerProps.getInstance().getProperty("QSKY", "token", "dev"))) {
            log.error("Plugin version of \"QSky2SenderPlugin\" not supported this version of QSystem cloud.");
            return;
        }
        log.debug("User to Sky: " + name + "(" + userId + "/" + branchId + ")");
        SkyService.getInstance().sendUserName(branchId, userId, name);
    }

    @Override
    public String getDescription() {
        return "Plugin \"QSky2SenderPlugin\". Send data to QSystem cloud.";
    }

    @Override
    public long getUID() {
        return UID;
    }
}
