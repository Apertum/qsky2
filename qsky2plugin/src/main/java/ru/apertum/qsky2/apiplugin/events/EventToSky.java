/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsky2.apiplugin.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.apertum.qsystem.common.CustomerState;
import ru.apertum.qsystem.common.model.QCustomer;
import ru.apertum.qsystem.server.model.QService;
import ru.apertum.qsystem.server.model.QUser;

/**
 * Класс-хранилище для передачи данных к месту отправки в облако
 *
 * @author egorov
 */
@AllArgsConstructor
@Data
public class EventToSky {

    private final QCustomer customer;
    private final QService service;
    private final QUser user;
    private final CustomerState customerState;
    private final Long newServiceId;

    public EventToSky(QCustomer customer, CustomerState customerState, Long newServiceId) {
        this.customer = customer;
        this.service = customer.getService();
        this.user = customer.getUser();
        this.customerState = customerState;
        this.newServiceId = newServiceId;
    }
}
