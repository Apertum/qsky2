/*
 *  Copyright (C) 2010 {Apertum}Projects. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.apertum.qsky2.apiplugin.events;

import lombok.extern.log4j.Log4j2;
import ru.apertum.qsky2.apiplugin.IQSkyPluginUID;
import ru.apertum.qsky2.apiplugin.ws.QskyAuth;
import ru.apertum.qsky2.apiplugin.ws.SkyService;
import ru.apertum.qsystem.common.exceptions.ServerException;
import ru.apertum.qsystem.extra.IPing;
import ru.apertum.qsystem.server.ServerProps;

import java.io.InputStream;
import java.util.Properties;

import static ru.apertum.qsky2.apiplugin.ws.QskyAuth.RESOURCE;

/**
 * @author egorov
 */
@Log4j2
public class Pinger implements IPing, IQSkyPluginUID {

    @Override
    public String getDescription() {
        final Properties settings = new Properties();
        final InputStream inStream = settings.getClass().getResourceAsStream(RESOURCE);
        try {
            settings.load(inStream);
        } catch (Exception ex) {
            log.error("Error reading description of QSky2SenderPlugin from {}.", RESOURCE);
            return "Error reading description of QSky2SenderPlugin.";
        }
        return "Plugin \"QSky2SenderPlugin\" v.=" + settings.getProperty("version") + " date=" + settings.getProperty("date") + " for QSystem cloud.";
    }

    @Override
    public int ping() {
        final Properties settings = new Properties();
        final InputStream inStream = settings.getClass().getResourceAsStream(RESOURCE);
        try {
            settings.load(inStream);
        } catch (Exception ex) {
            throw new ServerException("Проблемы с чтением версии. ", ex);
        }
        final boolean ready = QskyAuth.getInstance().isReady(ServerProps.getInstance().getProperty("QSKY", "token", "dev"));
        if (ready) {
            final Integer pingRes = SkyService.getInstance().ping(settings.getProperty("version"));
            QskyAuth.getInstance().setRefresh();
            return pingRes;
        } else {
            final int status = QskyAuth.getInstance().getAuthResponse(ServerProps.getInstance().getProperty("QSKY", "token", "dev")).status;
            QskyAuth.getInstance().setRefresh();
            return status;
        }
    }

    @Override
    public long getUID() {
        return UID;
    }
}
