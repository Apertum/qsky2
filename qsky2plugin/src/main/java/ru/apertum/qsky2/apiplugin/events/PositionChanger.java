/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsky2.apiplugin.events;

import lombok.extern.log4j.Log4j2;
import ru.apertum.qsky2.apiplugin.IQSkyPluginUID;
import ru.apertum.qsky2.apiplugin.core.PositionToSky;
import ru.apertum.qsky2.apiplugin.ws.QskyAuth;
import ru.apertum.qsky2.apiplugin.ws.WorkThreads;
import ru.apertum.qsystem.common.model.QCustomer;
import ru.apertum.qsystem.extra.ICustomerChangePosition;
import ru.apertum.qsystem.server.ServerProps;

/**
 * Этот класс создастся как плагин и методы его будут вызываться из главной программы как методы плагина
 *
 * @author egorov
 */
@Log4j2
public class PositionChanger implements ICustomerChangePosition, IQSkyPluginUID {

    @Override
    public void insert(QCustomer customer, QCustomer before, QCustomer after) {
        if (!QskyAuth.getInstance().isReady(ServerProps.getInstance().getProperty("QSKY", "token", "dev"))) {
            log.error("Версия плагина \"QSkySenderPlugin\" не сообветствует версии облака.");
            return;
        }
        // Создаем событие
        // Отсылаем событие
        WorkThreads.getInstance().moveClient(new PositionToSky(PositionToSky.Mode.INSERT, customer, before, after));
    }

    @Override
    public void remove(QCustomer customer) {
        if (!QskyAuth.getInstance().isReady(ServerProps.getInstance().getProperty("QSKY", "token", "dev"))) {
            log.error("Версия плагина \"QSkySenderPlugin\" не сообветствует версии облака.");
            return;
        }
        // Создаем событие
        // Отсылаем событие
        WorkThreads.getInstance().moveClient(new PositionToSky(PositionToSky.Mode.REMOVE, customer, null, null));
    }

    @Override
    public String getDescription() {
        return "Плагин \"QSkySenderPlugin\" отправляет в облако позицию клиента в очереди";
    }

    @Override
    public long getUID() {
        return UID;
    }
}
