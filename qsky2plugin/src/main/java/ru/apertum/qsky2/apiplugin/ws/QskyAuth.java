/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsky2.apiplugin.ws;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.log4j.Log4j2;
import ru.apertum.qsystem.common.exceptions.ServerException;

import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Класс проверки доступности облака
 *
 * @author egorov
 */
@Log4j2
public class QskyAuth {

    public static final String RESOURCE = "/qsky2-qsky2plugin.properties";

    private LoadingCache<String, SkyService.AuthResponse> authResult;

    private String version = "ver";

    private QskyAuth() {
        final Properties settings = new Properties();
        final InputStream inStream = settings.getClass().getResourceAsStream(RESOURCE);
        try {
            settings.load(inStream);
        } catch (Exception ex) {
            throw new ServerException("Проблемы с чтением версии. ", ex);
        }
        version = settings.getProperty("version");
        authResult = CacheBuilder.newBuilder().maximumSize(1)
                .expireAfterWrite(2, TimeUnit.HOURS).expireAfterAccess(30, TimeUnit.MINUTES)
                .build(new CacheLoader<String, SkyService.AuthResponse>() {
                    @Override
                    public SkyService.AuthResponse load(String token) {
                        try {
                            return SkyService.getInstance().auth(token, version);
                        } catch (Exception e) {
                            log.catching(e);
                            return new SkyService.AuthResponse(600, null, null);
                        }
                    }
                });
    }

    public static QskyAuth getInstance() {
        return PingResultHolder.INSTANCE;
    }

    public boolean isReady(String token) {
        try {
            return 200 == authResult.get(token).status;
        } catch (ExecutionException e) {
            log.catching(e);
            return false;
        }
    }

    public SkyService.AuthResponse getAuthResponse(String token) {
        try {
            return authResult.get(token);
        } catch (ExecutionException e) {
            log.catching(e);
            throw new ServerException(e);
        }
    }

    public void setRefresh() {
        authResult.invalidateAll();
    }

    private static class PingResultHolder {

        private static final QskyAuth INSTANCE = new QskyAuth();
    }
}
