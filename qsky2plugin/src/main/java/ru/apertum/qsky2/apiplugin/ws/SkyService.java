/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsky2.apiplugin.ws;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import lombok.extern.log4j.Log4j2;
import ru.apertum.qsystem.client.QProperties;
import ru.apertum.qsystem.common.GsonPool;
import ru.apertum.qsystem.common.QConfig;
import ru.apertum.qsystem.common.exceptions.QException;
import ru.apertum.qsystem.common.exceptions.ServerException;
import ru.apertum.qsystem.server.ServerProps;
import ru.apertum.qsystem.server.model.QProperty;

import javax.naming.AuthenticationException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Через этот класс используем коннектор до вебсервисов.
 *
 * @author egorov
 */
@Log4j2
public class SkyService {

    private static final String USER_AGENT = "User-Agent";
    private static final String BRANCH_ID = "branchId";
    private static final String SERVICE_ID = "serviceId";
    private static final String CUSTOMER_ID = "customerId";

    private SkyService() {
        // не требуется действий.
    }

    public static SkyService getInstance() {
        return SkyServiceHolder.INSTANCE;
    }

    // HTTP GET request
    private static String sendGet(String cmd, Map<String, Object> paramsRaw) throws Exception { // NOSONAR
        AuthResponse authResponse = QskyAuth.getInstance().getAuthResponse(ServerProps.getInstance().getProperty("QSKY", "token", "dev"));
        if (authResponse == null || authResponse.status != 200) {
            throw new ServerException("Not authorized.");
        }
        HashMap<String, String> headers = new HashMap<>();
        String tokenName = ServerProps.getInstance().getProperty("QSKY", "header-token-name", "qsky2-token");
        headers.put(tokenName, authResponse.token);
        log.trace("Invoke to {} params: {} - {}", authResponse.endpoint, paramsRaw, headers);
        try {
            return sendGet(authResponse.endpoint, cmd, paramsRaw, headers);
        } catch (AuthenticationException ex) {
            QskyAuth.getInstance().setRefresh();
            throw new QException("Auth error.", ex);
        }
    }

    /**
     * Напррямую этот метод дергается только аутентификацией.
     */
    private static String sendGet(final String skyServerUrl, String cmd, Map<String, Object> paramsRaw, Map<String, String> headers) throws Exception { // NOSONAR
        final Map<String, String> params = new HashMap<>();
        for (Map.Entry<String, Object> entry : paramsRaw.entrySet()) {
            if (entry.getValue() != null) {
                params.put(entry.getKey(), URLEncoder.encode(entry.getValue().toString(), StandardCharsets.UTF_8.name()));
            }
        }
        String pstr = params.entrySet().stream().map(e -> e.getKey() + "=" + e.getValue()).collect(Collectors.joining("&"));
        final String url = skyServerUrl + (skyServerUrl.endsWith("/") || cmd.startsWith("/") ? "" : "/") + cmd + "?" + pstr;
        log.trace("HTTP GET request on url: {}", url);
        final URL obj = new URL(url);
        final Proxy proxy = getProxy(Proxy.Type.HTTP);

        final HttpURLConnection con = (HttpURLConnection) (proxy == null
                ? (QConfig.cfg().doNotUseProxy() ? obj.openConnection(Proxy.NO_PROXY) : obj.openConnection())
                : obj.openConnection(proxy));
        final StringBuilder response;
        try {
            con.setRequestMethod("GET");
            //add reuqest header
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                if (entry.getValue() != null) {
                    con.setRequestProperty(entry.getKey(), entry.getValue());
                }
            }

            if (con.getResponseCode() != 200) {
                log.error("HTTP GET response code = " + con.getResponseCode());
                if (con.getResponseCode() / 100 == 4) {
                    throw new AuthenticationException("HTTP GET response code = " + con.getResponseCode());
                }
                throw new QException("HTTP GET response code = " + con.getResponseCode());
            }
            response = readResponseFromStream(con.getInputStream());
        } finally {
            con.disconnect();
        }

        //result
        final String res = response.toString();
        response.setLength(0);
        log.trace("HTTP GET response:\n" + res);
        return res;
    }

    private static class SkyServiceHolder {

        private static final SkyService INSTANCE = new SkyService();
    }

    public AuthResponse auth(String token, String version) throws Exception {
        final String skyServerUrl = ServerProps.getInstance().getProps().getSkyServerUrl();
        final HashMap<String, String> headers = new HashMap<>();
        String tokenName = ServerProps.getInstance().getProperty("QSKY", "header-authtoken-name", "qsky2-auth-token");
        String verName = ServerProps.getInstance().getProperty("QSKY", "header-ver-name", "qsky2-plugin-version");
        headers.put(tokenName, token);
        headers.put(verName, version);
        log.trace("Auth to {} params: {}", skyServerUrl, headers);
        final String res = sendGet(skyServerUrl, "", new HashMap<>(), headers);
        final Gson gson = GsonPool.getInstance().borrowGson();
        try {
            final AuthResponse response = gson.fromJson(res, AuthResponse.class);
            log.debug("Auth Response = {}", response);
            return response;
        } finally {
            GsonPool.getInstance().returnGson(gson);
        }
    }

    public static class AuthResponse {
        @Expose
        public int status;
        @Expose
        public String token;
        @Expose
        public String endpoint;

        public AuthResponse(int status, String token, String endpoint) {
            this.status = status;
            this.token = token;
            this.endpoint = endpoint;
        }

        public AuthResponse() {
        }

        @Override
        public String toString() {
            return String.format("[%d, %s, %s]", status, token, endpoint);
        }
    }

    public void changeCustomerStatus(Long branchId, Long serviceId, Long employeeId, Long customerId, Integer status, Integer number, String prefix) {
        final Map<String, Object> params = new HashMap<>();
        params.put(BRANCH_ID, branchId);
        params.put(SERVICE_ID, serviceId);
        params.put("employeeId", employeeId);
        params.put(CUSTOMER_ID, customerId);
        params.put("status", status);
        params.put("number", number);
        params.put("prefix", prefix);
        try {
            sendGet("changeCustomerStatus", params);
        } catch (Exception ex) {
            log.catching(ex);
        }
    }

    public void insertCustomer(Long branchId, Long serviceId, Long customerId, Long beforeCustId, Long afterCustId) {
        final Map<String, Object> params = new HashMap<>();
        params.put(BRANCH_ID, branchId);
        params.put(SERVICE_ID, serviceId);
        params.put(CUSTOMER_ID, customerId);
        params.put("beforeCustId", beforeCustId);
        params.put("afterCustId", afterCustId);
        try {
            sendGet("insertCustomer", params);
        } catch (Exception ex) {
            log.catching(ex);
        }
    }

    public void removeCustomer(Long branchId, Long serviceId, Long customerId) {
        final Map<String, Object> params = new HashMap<>();
        params.put(BRANCH_ID, branchId);
        params.put(SERVICE_ID, serviceId);
        params.put(CUSTOMER_ID, customerId);
        try {
            sendGet("removeCustomer", params);
        } catch (Exception ex) {
            log.catching(ex);
        }
    }

    public void sendServiceName(Long branchId, Long serviceId, String name) {
        final Map<String, Object> params = new HashMap<>();
        params.put(BRANCH_ID, branchId);
        params.put(SERVICE_ID, serviceId);
        params.put("name", name);
        try {
            sendGet("sendServiceName", params);
        } catch (Exception ex) {
            log.catching(ex);
        }
    }

    public void sendUserName(Long branchId, Long employeeId, String name) {
        final Map<String, Object> params = new HashMap<>();
        params.put(BRANCH_ID, branchId);
        params.put("employeeId", employeeId);
        params.put("name", name);
        try {
            sendGet("sendUserName", params);
        } catch (Exception ex) {
            log.catching(ex);
        }
    }

    public Integer ping(String version) {
        log.info("Ping version={}", version);
        String ping;
        try {
            ping = sendGet("ping", Collections.singletonMap("version", version));
        } catch (Exception ex) {
            log.error(ex);
            ping = "-3";
        }
        return ping.matches("-?\\d+") ? Integer.parseInt(ping) : -2;
    }

    private static Proxy getProxy(Proxy.Type proxyType) {
        if (QConfig.cfg().getProxy() != null) {
            log.trace("Proxy.Type." + proxyType + ": " + QConfig.cfg().getProxy().host + ":" + QConfig.cfg().getProxy().port);
            return new Proxy(proxyType, new InetSocketAddress(QConfig.cfg().getProxy().host, QConfig.cfg().getProxy().port));
        }
        QProperty hostname = QProperties.get().getProperty("proxy", "hostname");
        QProperty port = QProperties.get().getProperty("proxy", "port");
        if (hostname != null && port != null) {
            log.trace("Proxy.Type." + proxyType + ": " + hostname.getValue()
                    + ":" + port.getValueAsInt());
            return new Proxy(proxyType, new InetSocketAddress(hostname.getValue(),
                    port.getValueAsInt()));
        } else {
            return null;
        }
    }

    private static StringBuilder readResponseFromStream(InputStream inputStream) throws IOException {
        try (BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
            String inputLine;
            final StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            return response;
        }
    }
}
