package ru.apertum.qsky2.api;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.apertum.qsky2.model.Branch;
import ru.apertum.qsky2.model.Customer;
import ru.apertum.qsky2.model.Element;
import ru.apertum.qsky2.model.Employee;
import ru.apertum.qsky2.model.QSkySrv;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
@Transactional
@AllArgsConstructor
public class ApiRepo {

    @PersistenceContext
    private final EntityManager em;

    public Customer getCustomer(Long branchId, Long customerId) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Customer> query = builder.createQuery(Customer.class);
        Root<Customer> root = query.from(Customer.class);
        query.select(root).where(builder.and(builder.equal(root.get("branchId"), branchId), builder.equal(root.get("customerId"), customerId))).distinct(true);
        TypedQuery<Customer> q = em.createQuery(query);
        List<Customer> resultList = q.getResultList();
        return resultList.isEmpty() ? null : resultList.get(0);
    }

    public Employee getEmployee(Long branchId, Long employeeId) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Employee> query = builder.createQuery(Employee.class);
        Root<Employee> root = query.from(Employee.class);
        query.select(root).where(builder.and(builder.equal(root.get("branchId"), branchId), builder.equal(root.get("employeeId"), employeeId))).distinct(true);
        TypedQuery<Employee> q = em.createQuery(query);
        List<Employee> resultList = q.getResultList();
        return resultList.isEmpty() ? null : resultList.get(0);
    }

    public QSkySrv getService(Long branchId, Long serviceId) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<QSkySrv> query = builder.createQuery(QSkySrv.class);
        Root<QSkySrv> root = query.from(QSkySrv.class);
        query.select(root).where(builder.and(builder.equal(root.get("branchId"), branchId), builder.equal(root.get("serviceId"), serviceId))).distinct(true);
        TypedQuery<QSkySrv> q = em.createQuery(query);
        List<QSkySrv> resultList = q.getResultList();
        return resultList.isEmpty() ? null : resultList.get(0);
    }

    public Branch getBranch(Long branchId) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Branch> query = builder.createQuery(Branch.class);
        Root<Branch> root = query.from(Branch.class);
        query.select(root).where(builder.equal(root.get("branchId"), branchId)).distinct(true);
        TypedQuery<Branch> q = em.createQuery(query);
        List<Branch> resultList = q.getResultList();
        return resultList.isEmpty() ? null : resultList.get(0);
    }

    public void saveOrUpdate(Object entity) {
        // тут что-то не понятное творится. Вылетает из контекста сущность и просерается ID автоинкрементно проставленное.
        Element obj = (Element) (em.contains(entity) ? entity : em.merge(entity));
        em.persist(obj);
        ((Element) entity).setId(obj.getId());
    }
}
