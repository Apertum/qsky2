/*
 *  Copyright (C) 2010 {Apertum}Projects. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.apertum.qsky2.api;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import ru.apertum.qsky2.model.Branch;
import ru.apertum.qsky2.model.Customer;
import ru.apertum.qsky2.model.CustomerState;
import ru.apertum.qsky2.model.Employee;
import ru.apertum.qsky2.model.QSkySrv;
import ru.apertum.qsky2.model.Step;

import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author egorov
 */
@org.springframework.stereotype.Service
@PropertySource(value = "classpath:qsky2-qsysapi.properties", name = "props", encoding = "utf8")
@RequiredArgsConstructor
@Log4j2
public class ApiService {

    @Value("${qsky2.api.version}")
    private Integer version;

    @Value("${version}")
    private String versionProp;

    @Value("${qsky2.api.supported}")
    private String supported;

    private final ApiRepo repo;

    public synchronized void changeCustomerStatus(Long branchId, Long serviceId, Long employeeId, Long customerId, Integer status, Integer number, String prefix) {
        log.info(branchId + "  ser-" + serviceId + "  usr-" + employeeId + "  cust-" + customerId + "  #" + status + "  №" + prefix + number);

        if (status >= CustomerState.values().length) {
            log.warn("Status {} is strange in list {}.", status, CustomerState.values());
        } else {

            final CustomerState cs = CustomerState.values()[status];
            switch (cs) {
                //0 удален по неявке
                case STATE_DEAD:
                    kickCustomer(branchId, serviceId, customerId, employeeId, status);
                    break;

                // 1 стоит и ждет в очереди
                case STATE_WAIT:
                    standInService(branchId, serviceId, customerId, status, number, prefix);
                    break;

                // 2 стоит и ждет в очереди после того, как отлежался в отложенных положенное время и автоматически отправился в прежнюю очередь с повышенным приоритетом
                case STATE_WAIT_AFTER_POSTPONED:
                    moveToWaitCustomerAfterPostpone(branchId, customerId, serviceId, status);
                    break;

                // 3 Кастомер был опять поставлен в очередь т.к. услуга комплекстая и ждет с номером
                case STATE_WAIT_COMPLEX_SERVICE:
                    moveToWaitNextComplexService(branchId, customerId, serviceId, employeeId, status);
                    break;

                // 4 пригласили
                case STATE_INVITED:
                    inviteCustomer(branchId, serviceId, customerId, status, number, prefix, employeeId);
                    break;

                // 5 пригласили повторно в цепочке обработки. т.е. клиент вызван к оператору не первый раз а после редиректа или отложенности
                case STATE_INVITED_SECONDARY:
                    inviteSecondary(branchId, customerId, serviceId, employeeId, status);
                    break;

                // 6 отправили в другую очередь, идет как бы по редиректу в верх. Стоит ждет к новой услуге.
                case STATE_REDIRECT:
                    redirectCustomer(branchId, customerId, employeeId, serviceId, status);
                    break;

                // 7 начали с ним работать
                case STATE_WORK:
                    startWorkWithCustomer(branchId, customerId, serviceId, employeeId, status);
                    break;

                // 8 начали с ним работать повторно в цепочке обработки
                case STATE_WORK_SECONDARY:
                    startWorkSecondary(branchId, customerId, serviceId, employeeId, status);
                    break;

                // 9 состояние когда кастомер возвращается к прежней услуге после редиректа,
                // по редиректу в низ. Стоит ждет к старой услуге.
                case STATE_BACK:
                    backInService(branchId, customerId, employeeId, serviceId, status);
                    break;

                // 10 с кастомером закончили работать и он идет домой
                case STATE_FINISH:
                    finishWorkWithCustomer(branchId, customerId, employeeId, status);
                    break;

                // 11 с кастомером закончили работать и поместили в отложенные. домой не идет, сидит ждет покуда не вызовут.
                case STATE_POSTPONED:
                    customerToPostponed(branchId, customerId, employeeId, status);
                    break;

                default:
                    throw new AssertionError();
            }
        }
    }

    public void standInService(Long branchId, Long serviceId, Long customerId, Integer status, Integer number, String prefix) {
        log.info("Start standInService. branchId={}, serviceId={}, customerId={}, status={}, number={}, prefix={}", branchId, serviceId, customerId, status, number, prefix);

        Customer customer = repo.getCustomer(branchId, customerId);
        if (customer == null) {
            customer = new Customer(branchId, customerId);
        }
        if (serviceId != null && serviceId > 0) {
            customer.setServiceId(serviceId);
        }
        customer.setNumber(number);
        customer.setPrefix(prefix);
        customer.setState(status);

        final Step firstStep = new Step(branchId, customerId);
        firstStep.setServiceId(serviceId);
        firstStep.setStandTime(new Date());
        firstStep.setStartState(status);
        customer.setFirstStep(firstStep);
        repo.saveOrUpdate(customer);
        log.info("Finish standInService");
    }

    public void kickCustomer(Long branchId, Long serviceId, Long customerId, Long employeeId, Integer status) {
        log.info("Start kickCustomer. branchId={}, serviceId={}, customerId={},  employeeId={}, status={}", branchId, serviceId, customerId, employeeId, status);

        Customer customer = repo.getCustomer(branchId, customerId);
        if (customer == null) {
            log.error("ERROR: Customer not found id={}; branch={}", customerId, branchId);
            return;
        }
        customer.setState(status);

        if (customer.getFirstStep() != null) {
            final Step step = customer.getFirstStep().getLastStep();
            step.setFinishState(status);
            step.setFinishTime(new Date());
            step.setEmployeeId(employeeId);
            repo.saveOrUpdate(step);
        }
        repo.saveOrUpdate(customer);

        log.info("Finish kickCustomer");
    }

    public void inviteCustomer(Long branchId, Long serviceId, Long customerId, Integer status, Integer number, String prefix, Long employeeId) {
        log.info("Start inviteCustomer. branchId={}, serviceId={}, customerId={}, status={}, number={}, prefix={}", branchId, serviceId, customerId, status, number, prefix);

        Customer customer = null;

        customer = repo.getCustomer(branchId, customerId);
        if (customer == null) {
            // мог быть вызван по услуге-рулону.
            log.info("ERROR: Customer not found id={}", customerId);
            standInService(branchId, serviceId, customerId, status, number, prefix);
            customer = repo.getCustomer(branchId, customerId);
            if (customer == null) {
                log.error("ERROR: Customer not found id={}; branch={}", customerId, branchId);
                return;
            }
        }


        customer.setState(status);
        customer.setEmployeeId(employeeId);

        repo.saveOrUpdate(customer);

        log.info("Finish inviteCustomer");
    }

    public void inviteSecondary(Long branchId, Long customerId, Long serviceId, Long employeeId, Integer status) {
        log.info("Start inviteSecondary. branchId={}, customerId={},  serviceId={}, employeeId={}, status={}", branchId, customerId, serviceId, employeeId, status);

        Customer customer = repo.getCustomer(branchId, customerId);
        if (customer == null) {
            log.error("ERROR: Customer not found id={}; branch={}", customerId, branchId);
            return;
        }
        customer.setState(status);
        customer.setEmployeeId(employeeId);
        repo.saveOrUpdate(customer);

        log.info("Finish inviteSecondary");
    }

    public void startWorkWithCustomer(Long branchId, Long customerId, Long serviceId, Long employeeId, Integer status) {
        log.info("Start startWorkWithCustomer. branchId={}, customerId={},  serviceId={}, employeeId={}, status={}", branchId, customerId, serviceId, employeeId, status);

        Customer customer = repo.getCustomer(branchId, customerId);
        if (customer == null) {
            log.error("ERROR: Customer not found id={}; branch={}", customerId, branchId);
            return;
        }
        if (serviceId != null && serviceId > 0) {
            customer.setServiceId(serviceId);
        }
        customer.setState(status);

        final Step step = customer.getFirstStep().getLastStep();
        step.setEmployeeId(employeeId);
        step.setServiceId(serviceId);
        //step.setStartState(Customer.States.WORK_FIRST);
        step.setStartTime(new Date());
        step.setWaiting(step.getStartTime().getTime() - step.getStandTime().getTime());
        customer.setWaiting((customer.getWaiting() * (customer.getFirstStep().getStepsCount() - 1) + step.getWaiting()) / customer.getFirstStep().getStepsCount());

        repo.saveOrUpdate(step);
        repo.saveOrUpdate(customer);

        log.info("Finish startWorkWithCustomer");
    }

    public void startWorkSecondary(Long branchId, Long customerId, Long serviceId, Long employeeId, Integer status) {
        log.info("Start startWorkSecondary. branchId={}, customerId={},  serviceId={}, employeeId={}, status={}", branchId, customerId, serviceId, employeeId, status);

        Customer customer = repo.getCustomer(branchId, customerId);
        if (customer == null) {
            log.error("ERROR: Customer not found id={}; branch={}", customerId, branchId);
            return;
        }
        if (serviceId != null && serviceId > 0) {
            customer.setServiceId(serviceId);
        }
        customer.setState(status);

        final Step step = customer.getFirstStep().getLastStep();
        step.setEmployeeId(employeeId);
        step.setServiceId(serviceId);
        step.setStartTime(new Date());
        step.setWaiting(step.getStartTime().getTime() - step.getStandTime().getTime());
        customer.setWaiting((customer.getWaiting() * (customer.getFirstStep().getStepsCount() - 1) + step.getWaiting()) / customer.getFirstStep().getStepsCount());

        repo.saveOrUpdate(step);
        repo.saveOrUpdate(customer);

        log.info("Finish startWorkSecondary");
    }

    public void customerToPostponed(Long branchId, Long customerId, Long employeeId, Integer status) {
        log.info("Start customerToPostponed. branchId={}, customerId={},  employeeId={}, status={}", branchId, customerId, employeeId, status);
        Customer customer = repo.getCustomer(branchId, customerId);
        if (customer == null) {
            log.error("ERROR: Customer not found id={}; branch={}", customerId, branchId);
            return;
        }
        customer.setState(status);

        final Step step = customer.getFirstStep().getLastStep();
        step.setEmployeeId(employeeId);
        step.setFinishTime(new Date());
        step.setFinishState(status);
        step.setWorking(step.getFinishTime().getTime() - step.getStartTime().getTime());
        customer.setWorking((customer.getWorking() * (customer.getFirstStep().getStepsCount() - 1) + step.getWorking()) / customer.getFirstStep().getStepsCount());

        final Step postponedStep = new Step(branchId, customerId);
        postponedStep.setStandTime(new Date());
        postponedStep.setStartState(status);
        step.setAfter(postponedStep);
        postponedStep.setBefore(step);

        repo.saveOrUpdate(postponedStep);
        repo.saveOrUpdate(step);
        repo.saveOrUpdate(customer);
        log.info("Finish customerToPostponed");
    }

    public void redirectCustomer(Long branchId, Long customerId, Long employeeId, Long serviceId, Integer status) {
        log.info("Start redirectCustomer. branchId={}, customerId={},  employeeId={}, serviceId={}, status={}", branchId, customerId, employeeId, serviceId, status);
        Customer customer = repo.getCustomer(branchId, customerId);
        if (customer == null) {
            log.error("ERROR: Customer not found id={}; branch={}", customerId, branchId);
            return;
        }
        customer.setState(status);
        customer.setServiceId(serviceId);

        final Step step = customer.getFirstStep().getLastStep();
        step.setEmployeeId(employeeId);
        step.setFinishTime(new Date());
        step.setFinishState(status);
        step.setWorking(step.getFinishTime().getTime() - step.getStartTime().getTime());
        customer.setWorking((customer.getWorking() * (customer.getFirstStep().getStepsCount() - 1) + step.getWorking()) / customer.getFirstStep().getStepsCount());

        final Step redirectedStep = new Step(branchId, customerId);
        redirectedStep.setStandTime(new Date());
        redirectedStep.setStartState(status);
        redirectedStep.setServiceId(serviceId);
        step.setAfter(redirectedStep);
        redirectedStep.setBefore(step);

        repo.saveOrUpdate(redirectedStep);
        repo.saveOrUpdate(step);
        repo.saveOrUpdate(customer);
        log.info("Finish redirectCustomer");
    }

    public void moveToWaitCustomerAfterPostpone(Long branchId, Long customerId, Long serviceId, Integer status) {
        log.info("Start moveToWaitCustomerAfterPostpone. branchId={}, customerId={}, serviceId={}, status={}", branchId, customerId, serviceId, status);
        Customer customer = repo.getCustomer(branchId, customerId);
        if (customer == null) {
            log.error("ERROR: Customer not found id=" + customerId);
            return;
        }
        customer.setState(status);
        customer.setServiceId(serviceId);

        final Step step = customer.getFirstStep().getLastStep();
        step.setFinishTime(new Date());
        step.setFinishState(status);
        step.setWaiting(step.getFinishTime().getTime() - step.getStartTime().getTime());
        customer.setWaiting((customer.getWaiting() * (customer.getFirstStep().getStepsCount() - 1) + step.getWaiting()) / customer.getFirstStep().getStepsCount());

        final Step waitStep = new Step(branchId, customerId);
        waitStep.setStandTime(new Date());
        waitStep.setStartState(status);
        waitStep.setServiceId(serviceId);
        step.setAfter(waitStep);
        waitStep.setBefore(step);

        repo.saveOrUpdate(waitStep);
        repo.saveOrUpdate(step);
        repo.saveOrUpdate(customer);

        log.info("Finish moveToWaitCustomerAfterPostpone");
    }

    public void moveToWaitNextComplexService(Long branchId, Long customerId, Long serviceId, Long employeeId, Integer status) {
        log.info("Start moveToWaitNextComplexService. branchId={}, customerId={}, serviceId={}, employeeId={}, status={}", branchId, customerId, serviceId, employeeId, status);
        Customer customer = repo.getCustomer(branchId, customerId);
        if (customer == null) {
            log.error("ERROR: Customer not found id={}; branch={}", customerId, branchId);
            return;
        }
        customer.setState(status);
        customer.setServiceId(serviceId);

        final Step step = customer.getFirstStep().getLastStep();
        step.setEmployeeId(employeeId);
        step.setFinishTime(new Date());
        step.setFinishState(status);
        step.setWorking(step.getFinishTime().getTime() - step.getStartTime().getTime());
        customer.setWorking((customer.getWorking() * (customer.getFirstStep().getStepsCount() - 1) + step.getWorking()) / customer.getFirstStep().getStepsCount());

        final Step nextComplexStep = new Step(branchId, customerId);
        nextComplexStep.setStandTime(new Date());
        nextComplexStep.setStartState(status);
        nextComplexStep.setServiceId(serviceId);
        step.setAfter(nextComplexStep);
        nextComplexStep.setBefore(step);

        repo.saveOrUpdate(nextComplexStep);
        repo.saveOrUpdate(step);
        repo.saveOrUpdate(customer);

        log.info("Finish moveToWaitNextComplexService");
    }

    public void backInService(Long branchId, Long customerId, Long employeeId, Long serviceId, Integer status) {
        log.info("Start backInService. branchId={}, customerId={},  employeeId={}, serviceId={}, status={}", branchId, customerId, employeeId, serviceId, status);
        Customer customer = repo.getCustomer(branchId, customerId);
        if (customer == null) {
            log.error("ERROR: Customer not found id={}; branch={}", customerId, branchId);
            return;
        }
        customer.setState(status);
        customer.setServiceId(serviceId);

        final Step step = customer.getFirstStep().getLastStep();
        step.setEmployeeId(employeeId);
        step.setFinishTime(new Date());
        step.setFinishState(status);
        step.setWorking(step.getFinishTime().getTime() - step.getStartTime().getTime());
        customer.setWorking((customer.getWorking() * (customer.getFirstStep().getStepsCount() - 1) + step.getWorking()) / customer.getFirstStep().getStepsCount());

        final Step redirectedStep = new Step(branchId, customerId);
        redirectedStep.setStandTime(new Date());
        redirectedStep.setStartState(status);
        redirectedStep.setServiceId(serviceId);
        step.setAfter(redirectedStep);
        redirectedStep.setBefore(step);

        repo.saveOrUpdate(redirectedStep);
        repo.saveOrUpdate(step);
        repo.saveOrUpdate(customer);
        log.info("Finish backInService");
    }

    public void finishWorkWithCustomer(Long branchId, Long customerId, Long employeeId, Integer status) {
        log.info("Start finishWorkWithCustomer. branchId={}, customerId={},  employeeId={},  status={}", branchId, customerId, employeeId, status);
        Customer customer = repo.getCustomer(branchId, customerId);
        if (customer == null) {
            log.error("ERROR: Customer not found id={}; branch={}", customerId, branchId);
            return;
        }
        customer.setState(status);
        final Step step = customer.getFirstStep().getLastStep();
        step.setEmployeeId(employeeId);
        step.setFinishState(status);
        step.setFinishTime(new Date());
        if (step.getStartTime() == null) {
            log.error("Error! step.getStartTime()=NULL");
            step.setWorking(5L);
        } else {
            step.setWorking(step.getFinishTime().getTime() - step.getStartTime().getTime());
        }
        customer.setWorking((customer.getWorking() * (customer.getFirstStep().getStepsCount() - 1) + step.getWorking()) / customer.getFirstStep().getStepsCount());
        repo.saveOrUpdate(step);
        repo.saveOrUpdate(customer);
        log.info("Finish finishWorkWithCustomer");
    }

    public synchronized void insertCustomer(Long branchId, Long serviceId, Long customerId, Long beforeCustId, Long afterCustId) {
        log.info("Start insertCustomer. branchId={},  serviceId={},  customerId={},  beforeCustId={},  afterCustId={}", branchId, serviceId, customerId, beforeCustId, afterCustId);
        Customer customer = repo.getCustomer(branchId, customerId);
        if (customer == null) {
            customer = new Customer(branchId, customerId);
            if (serviceId != null && serviceId > 0) {
                customer.setServiceId(serviceId);
            }
        }
        final Customer before = beforeCustId == null ? null : repo.getCustomer(branchId, beforeCustId);
        final Customer after = afterCustId == null ? null : repo.getCustomer(branchId, afterCustId);
        if (before != null) {
            before.setAfter(customer);
            customer.setBefore(before);
        }
        if (after != null) {
            after.setBefore(customer);
            customer.setAfter(after);
        }

        repo.saveOrUpdate(customer);
        if (after != null) {
            repo.saveOrUpdate(after);
        }
        if (before != null) {
            repo.saveOrUpdate(before);
        }
        log.info("Finish insertCustomer");
    }

    public synchronized void removeCustomer(Long branchId, Long serviceId, Long customerId) {
        log.info("Start removeCustomer. branchId={},  serviceId={},  customerId={}", branchId, serviceId, customerId);
        final Customer customer = repo.getCustomer(branchId, customerId);
        if (customer == null) {
            return;
        }
        if (customer.getBefore() != null) {
            customer.getBefore().setAfter(customer.getAfter());
        }
        if (customer.getAfter() != null) {
            customer.getAfter().setBefore(customer.getBefore());
        }
        customer.setAfter(null);
        customer.setBefore(null);

        if (customer.getBefore() != null) {
            repo.saveOrUpdate(customer.getBefore());
        }
        if (customer.getAfter() != null) {
            repo.saveOrUpdate(customer.getAfter());
        }
        repo.saveOrUpdate(customer);
        log.info("Finish removeCustomer");
    }

    public Integer ping(String pluginVersion) {
        if (versionProp.equals(pluginVersion)) {
            return version;
        }
        final boolean isSupportClient = Arrays.asList(supported.split(";")).contains(pluginVersion);
        log.info("Ping. Version={}; supported={}; result={}", pluginVersion, supported, isSupportClient);
        return isSupportClient ? version : -1;
    }

    public synchronized void sendServiceName(Long branchId, Long serviceId, String name) {
        log.info("Invoke sendServiceName. branchId={},  serviceId={},  name={}", branchId, serviceId, name);
        dataLock.lock();
        try {
            QSkySrv service = repo.getService(branchId, serviceId);
            if (service == null) {
                service = new QSkySrv(branchId, serviceId, name);

                Branch branch = repo.getBranch(branchId);
                if (branch == null) {
                    branch = new Branch("Autocreated");
                    branch.setBranchId(branchId);
                    branch.setActive(true);
                    branch.setTimeZone(0);
                    repo.saveOrUpdate(branch);
                }
            }
            service.setName(name);
            repo.saveOrUpdate(service);
        } finally {
            dataLock.unlock();
        }
    }

    private static final ReentrantLock dataLock = new ReentrantLock();

    public synchronized void sendUserName(Long branchId, Long employeeId, String name) {
        log.info("Invoke sendUserName. branchId={},  employeeId={},  name={}", branchId, employeeId, name);
        dataLock.lock();
        try {
            Employee employee = repo.getEmployee(branchId, employeeId);
            if (employee == null) {
                employee = new Employee(branchId, employeeId, name);

                Branch branch = repo.getBranch(branchId);
                if (branch == null) {
                    branch = new Branch("Autocreated");
                    branch.setBranchId(branchId);
                    branch.setActive(true);
                    branch.setTimeZone(0);
                    repo.saveOrUpdate(branch);
                }
            }
            employee.setName(name);
            repo.saveOrUpdate(employee);
        } finally {
            dataLock.unlock();
        }
    }
}