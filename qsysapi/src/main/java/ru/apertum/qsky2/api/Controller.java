package ru.apertum.qsky2.api;

import lombok.RequiredArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

@RequiredArgsConstructor
@RestController
@RequestMapping("/qsky2api")
public class Controller {

    private final ApiService service;

    @GetMapping(value = "changeCustomerStatus")
    public void changeCustomerStatus(@RequestParam(name = "branchId") Long branchId,
                                     @RequestParam(name = "serviceId") @Nullable Long serviceId,
                                     @RequestParam(name = "employeeId") @Nullable Long employeeId,
                                     @RequestParam(name = "customerId") Long customerId,
                                     @RequestParam(name = "status") Integer status,
                                     @RequestParam(name = "number") Integer number,
                                     @RequestParam(name = "prefix") @Nullable String prefix) throws UnsupportedEncodingException {
        service.changeCustomerStatus(branchId, serviceId, employeeId, customerId, status, number, prefix == null ? "" : URLDecoder.decode(prefix, StandardCharsets.UTF_8.name()));
    }

    @GetMapping(value = "insertCustomer")
    public void insertCustomer(@RequestParam(name = "branchId") Long branchId, @RequestParam(name = "serviceId") Long serviceId, @RequestParam(name = "customerId") Long customerId,
                               @RequestParam(name = "beforeCustId") @Nullable Long beforeCustId,
                               @RequestParam(name = "afterCustId") @Nullable Long afterCustId) {
        service.insertCustomer(branchId, serviceId, customerId, beforeCustId, afterCustId);
    }

    @GetMapping(value = "removeCustomer")
    public void removeCustomer(@RequestParam(name = "branchId") Long branchId, @RequestParam(name = "serviceId") Long serviceId, @RequestParam(name = "customerId") Long customerId) {
        service.removeCustomer(branchId, serviceId, customerId);
    }

    @GetMapping(value = "ping")
    public Integer ping(@RequestParam(name = "version") String version) {
        return service.ping(version);
    }

    @GetMapping(value = "sendServiceName")
    public void sendServiceName(@RequestParam(name = "branchId") Long branchId, @RequestParam(name = "serviceId") Long serviceId, @RequestParam(name = "name") String name) throws UnsupportedEncodingException {
        service.sendServiceName(branchId, serviceId, URLDecoder.decode(name, StandardCharsets.UTF_8.name()));
    }

    @GetMapping(value = "sendUserName")
    public void sendUserName(@RequestParam(name = "branchId") Long branchId, @RequestParam(name = "employeeId") Long employeeId, @RequestParam(name = "name") String name) throws UnsupportedEncodingException {
        service.sendUserName(branchId, employeeId, URLDecoder.decode(name, StandardCharsets.UTF_8.name()));
    }
}
