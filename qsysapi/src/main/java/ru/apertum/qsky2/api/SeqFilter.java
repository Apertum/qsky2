package ru.apertum.qsky2.api;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(1)
@RequiredArgsConstructor
@Log4j2
public class SeqFilter implements Filter {

    private final SeqService seqService;

    private FilterConfig filterConfig;

    @Bean
    public FilterRegistrationBean regFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(this);
        registrationBean.addUrlPatterns("/*");
        return registrationBean;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        final String path = httpRequest.getRequestURI();
        if (path.endsWith("/ping/")) { // Пинг не пробиваем по аутентификации. Пинг так же служит для проверки версии.
            chain.doFilter(request, response); // Just continue chain.
            return;
        }
        final SeqService.Access permission = seqService.permission(httpRequest.getHeader(seqService.getHeaderNameIn()), httpRequest.getRemoteHost());
        if (permission.isSuccess()) {
            chain.doFilter(request, response);
        } else {
            log.warn("Request was rejected by {}", permission);
            httpResponse.sendError(permission.getStatus(), permission.getMessage());
        }
    }

    @Override
    public void destroy() {
        //noting
    }

    @Override
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }
}
