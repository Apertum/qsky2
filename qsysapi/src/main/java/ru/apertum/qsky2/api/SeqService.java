package ru.apertum.qsky2.api;


import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
@Log4j2
public class SeqService {

    @Value("${qsky2.auth.header-name-in}")
    @Getter
    private String headerNameIn;

    @Value("${qsky2.auth.header-name-out}")
    @Getter
    private String headerNameOut;

    @Value("${qsky2.auth.url}")
    @Getter
    private String authUrl;

    private LoadingCache<Requester, Access> tokens;

    /**
     * Проверка доступности сервиса.
     *
     * @param token      Значение http параметра.
     * @param remoteHost откуда пришел запрос.
     * @return аутенцифицирован или нет.
     */
    public Access permission(String token, String remoteHost) {
        if (token == null || token.isEmpty()) {
            return Access.AUTH_REQUIRED;
        }
        try {
            return tokens.get(new Requester(token, remoteHost));
        } catch (ExecutionException e) {
            log.catching(e);
            return Access.ERROR;
        }
    }

    @PostConstruct
    private void init() {
        tokens = CacheBuilder.newBuilder().maximumSize(1_000).expireAfterWrite(10, TimeUnit.MINUTES).build(new CacheLoader<Requester, Access>() {
            @Override
            public Access load(Requester requester) {
                final HttpResponse response;
                try {
                    response = Request.Get(authUrl).addHeader(headerNameOut, requester.token).execute().returnResponse();
                } catch (Exception e) {
                    log.error(e);
                    return Access.ERROR;
                }
                if (response.getStatusLine().getStatusCode() == HttpServletResponse.SC_ACCEPTED) {
                    return Access.SUCCESS;
                } else {
                    log.warn("Access DENIED {} on {} with token: {}", response.getStatusLine().getStatusCode(), authUrl, requester.token);
                    return Access.DENIED;
                }
            }
        });

    }

    @Data
    public static class Access {
        private static final Access SUCCESS = new Access(true, "Success", HttpServletResponse.SC_OK);
        private static final Access DENIED = new Access(false, "Access denied!", HttpServletResponse.SC_FORBIDDEN);
        private static final Access AUTH_REQUIRED = new Access(false, "Unauthorized. Auth required!", HttpServletResponse.SC_UNAUTHORIZED);
        private static final Access UPGRADE_REQUIRED = new Access(false, "Upgrade required!", 426);
        private static final Access ERROR = new Access(false, "Error!", HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        private final boolean success;
        private final String message;
        private final int status;
    }

    @Data
    public static class Requester {
        private final String token;
        private final String remoteHost;
    }
}
